package java52thPractise.practiseDemo.somelittle.b;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Demo1 implements converter<Long, Date>{

    @Override
    public Date convert(Long l)  {
        SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");

        String format1 = format.format(l);
        Date date = null;
        try {
            date = format.parse(format1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(format1);
        System.out.println(date);
        return date;
    }


}
