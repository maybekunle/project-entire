package java52thPractise.practiseDemo.somelittle.regularexpression;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo1 {
    @Test
    public void run(){
        String qq = "411003575";
        // boolean right = checkIsQQ(qq);
        // System.out.println(right);
        System.out.println(qq.matches("[1-9]\\d{4,13}"));
    }

    private boolean checkIsQQ(String qq) {
        // 长度是否6-12位
        if(qq.length() < 6 || qq.length() >11){
            return false;
        }
        // 是否全是数字
        for (int i = 0; i < qq.length(); i++) {
            if(qq.charAt(i) < '0' || qq.charAt(i) > '9'){
                return false;
            }
        }
        // 不能是零开头
        if (qq.startsWith(String.valueOf('0'))){
            return false;
        }

        return true;
    }

    @Test
    public void testPhone(){

        // 验证手机号
        // 17302213090
        // 第一：开头必须是1   第二：第二位是35789 第三：后面九尾随意数字
        String s = "[1][35789]\\d{9}";
        System.out.println("17302213090".matches(s));
        System.out.println("18302213090".matches(s));
        System.out.println("14302213090".matches(s));
        System.out.println("1302213555090".matches(s));
        System.out.println("173022a5090".matches(s));
    }

    @Test
    public void testEmail(){
        // 411003075@qq.com   changbufang@xdf.edu.cn
        // 一：@ 左半部分，数字和字母随意，0不能开头  ||  任意的数字字母下划线，至少出现一次
                // \\w+
        // 二：@                                 ||  只出现一次
                // @
        // 三：@右半部分
                // .左半部分：2-6个字母            ||  [\\w&&[^_]]{2,6}

                // .                             ||  .
                // .右半部分：2-6个字母            ||  大小写字母都可以，只能出现2-3次    [a-zA-Z]{2,3}
        String s = "[^0]\\w*[@][a-zA-z]{2,6}[\\.[a-zA-z]{2,6}]{1,2}";
        String s2 = "\\w+@[\\w&&[^_]]{2,6}(\\.[a-zA-Z]{2,3}){1,2}";
        System.out.println("411003575@qq.com".matches(s2));
        System.out.println("411003575@qq.com.cn".matches(s2));
        System.out.println("哎3575@qq.com".matches(s2));
        System.out.println("哎_575@qq.com".matches(s2));
        System.out.println("_575@qq.com".matches(s2));
    }

    @Test
    public void testRegx(){
        String str = "Java自从95年面世以来，经历了很多版本，目前企业中时用的最多的就是Java8和Java11，" +
                "因为这两个是长期支持版本，下一个长期支持版本是Java17，相信在不久的未来Java17也会登上历史的舞台。";
        // 获取正则表达式对象
        Pattern p = Pattern.compile("Java\\d{0,2}");
        // 获取文本匹配器的对象
        Matcher m = p.matcher(str);

        // 循环读取
        while (m.find()){
            String s = m.group();
            System.out.println(s);
        }
    }
}
