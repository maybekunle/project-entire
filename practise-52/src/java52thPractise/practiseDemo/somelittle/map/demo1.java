package java52thPractise.practiseDemo.somelittle.map;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

public class demo1 {


    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();

        map.put("光头强", "熊大");
        map.put("汤姆", "杰克");
        map.put("灰太狼", "喜洋洋");
        map.put("海绵宝宝", "派大星");
        map.put("xr ", "派大星");
        map.put("翠花", "熊大");
        map.put("李白", "青莲剑歌");

        String s = map.get("给新湖");
        String orDefault1 = map.getOrDefault("黑心虎", "gun");
        String orDefault2 = map.getOrDefault("李白", "gun");
        System.out.println(orDefault1);
        System.out.println(orDefault2);
        System.out.println(s);


        System.out.println(map);

        // Set<String> keySet = map.keySet();
        //
        // for (String key : keySet) {
        //     String value = map.get(key);
        //     System.out.println(key + ":" + value);
        // }

        // Set<Map.Entry<String, String>> entries = map.entrySet();
        // for (Map.Entry<String, String> entry : entries) {
        //     String key = entry.getKey();
        //     String value = entry.getValue();
        //     System.out.println(key + ":" + value);
        // }




    }
}
