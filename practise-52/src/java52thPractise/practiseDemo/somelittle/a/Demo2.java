package java52thPractise.practiseDemo.somelittle.a;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class Demo2 {
    public static void main(String[] args) {
        File file = new File("d://1");

       // new (file,new FileFilter() ->)
        File[] files = file.listFiles((File pathname) -> pathname.getName().endsWith("txt"));
        System.out.println(Arrays.toString(files));
    }
}
