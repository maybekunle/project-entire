package java52thPractise.practiseDemo.somelittle.a;

import java.util.Arrays;
import java.util.Comparator;

public class Demo3 {
    public static void main(String[] args) {
        String[] strings = {"acxc","cdsdds","s","vsd","jh"};

        Student[] students = {
                new Student("光头强",18,"高一"),
                new Student("光头弱",22,"高三"),
                new Student("不光头强",15,"高一"),
                new Student("不光头弱",18,"高二"),
                new Student("光屁股强",25,"高二"),
                new Student("光屁股弱",18,"高一"),
        };
        System.out.println("排序前：");
        System.out.println(Arrays.toString(strings));

       // Arrays.sort(students,new MyMethod());

        Arrays.sort(students,new Comparator<Student>(){
            @Override
            public int compare(Student o1, Student o2) {
                return o1.age - o2.age;
            }
        });

        // Arrays.sort(strings, new Comparator<String>() {
        //     @Override
        //     public int compare(String o1, String o2) {
        //         return o1.compareTo(o2);
        //     }
        // });

        Arrays.sort(strings,((o1, o2) -> o1.length() - o2.length()));

        // new Comparator<Student>(){
        //     @Override
        //     public int compare(Student o1, Student o2) {
        //         return o1.age - o2.age;
        //     }
        // };

        System.out.println("排序后：");
        System.out.println(Arrays.toString(strings));

    }
}


//class Student implements Comparable<Student>{
class Student {
    String name;
    int age;
    String grade;

    public Student(String name, int age, String grade) {
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

    // public String getName() {
    //     return name;
    // }
    //
    // public int getAge() {
    //     return age;
    // }
    //
    // public String getGrade() {
    //     return grade;
    // }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", grade='").append(grade).append('\'');
        sb.append('}');
        return sb.toString();
    }


    // @Override
    // public int compareTo(Student o) {
    //     return this.age - o.age;
    // }
}

class MyMethod implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}