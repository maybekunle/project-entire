package java52thPractise.practiseDemo.somelittle.a;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClassTest {

        public static void main(String[] args) throws IOException {
            // - 创建发送端的socket对象
            //DatagramSocket(int port)
            // 创建数据报套接字并将其绑定到本地主机上的指定端口。
            DatagramSocket datagramSocket = new DatagramSocket(8888);

            //- 把要发送的数据封装成数据报包
            String s = "hello udp";
            byte[] bytes = s.getBytes();
            InetAddress targetIP = InetAddress.getByName("192.168.3.53");
            int port = 9999;

            // DatagramPacket(byte[] buf,  int offset, int length, InetAddress address, int port)
            // 构造数据报包，用来将长度为 length 偏移量为 offset  的包发送到指定主机上的指定端口号
            DatagramPacket sendPacket =
                    new DatagramPacket(bytes, 0, bytes.length, targetIP, port);

            //- send方法发送数据报包
            datagramSocket.send(sendPacket);
            //- 释放资源close
            datagramSocket.close();
        }

}
