package java52thPractise.practiseDemo.somelittle.a;

import java.util.ArrayList;

public class ArraylistDemo {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();

        list.add("da");
        list.add("干饭");
        list.add("小零下");
        list.add("da");
        list.add("sss");
        //list.add("sss");
        list.add("ff");

        list.remove("da");
        list.remove(4);
        list.remove(1);
        //list.remove("ff");

        System.out.println(list);
    }
}
