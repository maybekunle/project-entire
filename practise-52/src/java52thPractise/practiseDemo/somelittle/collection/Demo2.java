package java52thPractise.practiseDemo.somelittle.collection;

import java.util.ArrayList;
import java.util.Collection;

public class Demo2 {
    public static void main(String[] args) {
        Collection<Student> students = new ArrayList<>();
        students.add(new Student("王小明",18));
        students.add(new Student("李小红",17));
        students.add(new Student("赵小天",19));
        students.add(new Student("王大明",20));

        Student s = new Student("王小明",18);

        System.out.println(students.contains(s));

    }
}

class Student{
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (age != student.age) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }
}
