package java52thPractise.practiseDemo.somelittle.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class Demo1 {
    public static void main(String[] args) {
        Collection<String> con = new ArrayList<>();
        con.add("熊大");
        con.add("熊大");
        con.add("熊大");
        con.add("熊二");
        con.add("熊二");
        con.add("光头强");
        con.add("灰太狼");
        con.add("红猫");
        String s0 = "蓝兔";
        con.add(s0);
        System.out.println(con);

        long count = con.stream().count();
        System.out.println(count);


        System.out.println("============");

        Collection<String> cons = new ArrayList<>();
        cons.add("熊大");
        cons.add("熊大");
        cons.add("熊二");
        cons.add("光头强");
        cons.add("黑心虎");
        cons.add("麒麟");

        System.out.println(cons.containsAll(con));

        // Object[] objects = con.toArray();
        // for (Object object : objects) {
        //     System.out.println(object);
        // }
        // System.out.println("**********");
        // Object[] objects1 = cons.toArray(new String[4]);
        // for (Object o : objects1) {
        //     System.out.println(o);
        // }
        // Iterator<String> iterator = con.iterator();
        // while (iterator.hasNext()){
        //     String name = iterator.next();
        //     System.out.println(name);
        // }

        // 匿名内部类
        // con.forEach(new Consumer<String>() {
        //     @Override
        //     public void accept(String s) {
        //         System.out.println(s);
        //     }
        // });

        // lambda 表达式
        // con.forEach(s -> System.out.println(s));

        // System.out.println(con.size());

    }
}
