package java52thPractise.practiseDemo.licode;

// 给定链表的头结点 head ，请将其按 升序 排列并返回 排序后的链表 。
public class No077 {
    public static void main(String[] args) {
        Node a = new Node(1);
        Node b = new Node(6,a);
        Node c = new Node(4,b);
        Node d = new Node(2,c);
        Node e = new Node(5,d);

        Node head = sortList(e);

        Node head1 = e;
        while (head1 != null){
            System.out.print(head1.date + " ");
            head1 = head1.next;
        }
    }

    private static Node sortList(Node e) {


        return null;
    }
}

class Node {
    int date;
    Node next;

    public Node() {
    }

    public Node(int date) {
        this.date = date;
    }

    public Node(int date, Node next) {
        this.date = date;
        this.next = next;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("date=").append(date);
        sb.append(", next=").append(next);
        sb.append('}');
        return sb.toString();
    }
}