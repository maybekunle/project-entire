package java52thPractise.practiseDemo.licode;

import java.util.Arrays;
import java.util.Scanner;

public class No2241Second {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("您要存多少钱？");
        System.out.println("20的有：");
        int two = scanner.nextInt();
        System.out.println("50的有：");
        int five = scanner.nextInt();
        System.out.println("100的有：");
        int ten = scanner.nextInt();
        System.out.println("200的有：");
        int twenty = scanner.nextInt();
        System.out.println("500的有：");
        int fifty = scanner.nextInt();

        System.out.println("取多少？");
        int amount = scanner.nextInt();

        int[] banknotesCount = {two,five,ten,twenty,fifty};
        ATM atm = new ATM();
        atm.deposit(banknotesCount);
        int[] withdraw = atm.withdraw(amount);

        System.out.println("ATM取出来：" + Arrays.toString(withdraw));
        System.out.println("再取多少？");
        int amount2 = scanner.nextInt();
        int[] withdraw2 = atm.withdraw(amount2);

        System.out.println("ATM取出来：" + Arrays.toString(withdraw2));

    }
}

class ATM {

    private static long[] atmAmount;
    private static int[] money = {20,50,100,200,500};

    public ATM() {
        atmAmount = new long[5];
    }

    public void deposit(int[] banknotesCount) {

        for (int i = 0; i < 5; i++) {
            atmAmount[i] = atmAmount[i] + banknotesCount[i];
        }

    }

    public int[] withdraw(int amount) {
        int[] out = new int[5];
        for (int i = 4; i >=0 ; i--) {
            int k = amount / money[i];

            if(k <= atmAmount[i]){
                // atmAmount[i] = atmAmount[i] - k;//**
                out[i] = k;
                amount = (amount - money[i] * k);
            }else {
                out[i] = (int) atmAmount[i];
                // atmAmount[i] = 0;//**
                amount = (int) (amount - money[i] * atmAmount[i]);
            }

        }
        if(amount > 0){
            return new int[]{-1};
        }

        // hei,最后通过了再修改原数据，不用担心修改结束后最后没通过的问题
        for (int i = 0; i < 5; i++) {
            atmAmount[i] = atmAmount[i] - out[i];
        }
        return out;
    }

}