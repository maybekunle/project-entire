package java52thPractise.practiseDemo.licode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 * 一个 ATM 机器，存有种面值的钞票：20，50，100，200和500美元。
 * 初始时，ATM 机是空的。用户可以用它存或者取任意数目的钱。
 * 取款时，机器会优先取 较大数额的钱。
 *
 * 比方说，你想取$300，并且机器里有2张 $50的钞票，1张$100的钞票和1张$200的钞票，那么机器会取出$100 和$200的钞票。
 * 但是，如果你想取$600，机器里有3张$200的钞票和1张$500的钞票，那么取款请求会被拒绝，
 * 因为机器会先取出$500的钞票，然后无法取出剩余的$100。
 * 注意，因为有$500钞票的存在，机器不能取$200的钞票。
 * 请你实现 ATM 类：
 *
 * ATM()初始化 ATM 对象。
 * void deposit(int[] banknotesCount)分别存入$20，$50，$100，$200和$500钞票的数目。
 * int[] withdraw(int amount)返回一个长度为5的数组，分别表示$20，$50，$100，$200和$500钞票的数目，
 * 并且更新 ATM 机里取款后钞票的剩余数量。如果无法取出指定数额的钱，请返回[-1]（这种情况下不取出任何钞票）。
 */

public class No2241 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("您要存多少钱？");
        System.out.println("20的有：");
        int two = scanner.nextInt();
        System.out.println("50的有：");
        int five = scanner.nextInt();
        System.out.println("100的有：");
        int ten = scanner.nextInt();
        System.out.println("200的有：");
        int twenty = scanner.nextInt();
        System.out.println("500的有：");
        int fifty = scanner.nextInt();

        System.out.println("取多少？");
        int amount = scanner.nextInt();

        int[] banknotesCount = {two,five,ten,twenty,fifty};
        ATM2 atm = new ATM2();
        atm.deposit(banknotesCount);
        int[] withdraw = atm.withdraw(amount);

        System.out.println("ATM取出来：" + Arrays.toString(withdraw));
        System.out.println("再取多少？");
        int amount2 = scanner.nextInt();
        int[] withdraw2 = atm.withdraw(amount2);

        System.out.println("ATM取出来：" + Arrays.toString(withdraw2));


    }
}

class ATM2 {

    private static int[] atmAmount;

    public ATM2() {
        atmAmount = new int[5];
    }

    public void deposit(int[] banknotesCount) {

        atmAmount[0] = atmAmount[0] + banknotesCount[0];
        atmAmount[1] = atmAmount[1] + banknotesCount[1];
        atmAmount[2] = atmAmount[2] + banknotesCount[2];
        atmAmount[3] = atmAmount[3] + banknotesCount[3];
        atmAmount[4] = atmAmount[4] + banknotesCount[4];

    }

    public int[] withdraw(int amount) {
        int[] out ;

        int e = amount / 500;
        int d = (amount - 500 * e) / 200;
        int c = (amount - 500 * e - 200 * d) / 100;
        int b = (amount - 500 * e - 200 * d - 100 * c) / 50;
        int a = (amount - 500 * e - 200 * d - 100 * c - 50 * b) / 20;
        int x = (amount - 500 * e - 200 * d - 100 * c - 50 * b) % 20;
        if (x != 0) {
            return new int[]{-1};
        }
        if (atmAmount[4] >= e ) {
            atmAmount[4] = atmAmount[4] - e;
        } else {
            return new int[]{-1};
        }
        if (atmAmount[3] >= d) {
            atmAmount[3] = atmAmount[3] -  d;
        } else {
            atmAmount[4] = atmAmount[4] + e;
            return new int[]{-1};
        }
        if (atmAmount[2] >= c) {
            atmAmount[2] = atmAmount[2] -  c;
        } else {
            atmAmount[4] = atmAmount[4] + e;
            atmAmount[3] = atmAmount[3] +  d;
            return new int[]{-1};
        }
        if (atmAmount[1] >=  b) {
            atmAmount[1] = atmAmount[1] - b;
        } else {
            atmAmount[4] = atmAmount[4] + e;
            atmAmount[3] = atmAmount[3] +  d;
            atmAmount[2] = atmAmount[2] +  c;
            return new int[]{-1};
        }
        if (atmAmount[0] >=  a) {
            atmAmount[0] = atmAmount[0] -  a;
        } else {
            atmAmount[4] = atmAmount[4] + e;
            atmAmount[3] = atmAmount[3] +  d;
            atmAmount[2] = atmAmount[2] +  c;
            atmAmount[1] = atmAmount[1] + b;
            return new int[]{-1};
        }

        out = new int[]{a,b,c,d,e};

        return out;
    }

}