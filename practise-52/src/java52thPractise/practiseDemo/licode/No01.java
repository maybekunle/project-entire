package java52thPractise.practiseDemo.licode;

import java.util.Arrays;
import java.util.HashMap;

public class No01 {
    public static void main(String[] args) {
        int[] nums = {1,3,5,9};
        int target = 8;
        int[] num = findIndexOfTarget(nums,target);
        System.out.println(Arrays.toString(num));
    }

    private static int[] findIndexOfTarget(int[] nums, int target) {

        int[] num = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if(nums[i] + nums[j] == target){
                    num[0] = i;
                    num[1] = j;
                }

            }
        }
        return num;
    }
}
