package java52thPractise.practiseDemo.licode;

/**
 * 给你两个整数数组nums1 和nums2，两个数组长度都是n，再给你一个整数k。你可以对数组nums1进行以下操作：
 *
 * 选择两个下标i 和j，将nums1[i]增加k，将nums1[j]减少k。换言之，nums1[i] = nums1[i] + k 且nums1[j] = nums1[j] - k。
 * 如果对于所有满足0 <= i < n都有num1[i] == nums2[i]，那么我们称nums1 等于nums2。
 *
 * 请你返回使nums1 等于nums2的最少操作数。如果没办法让它们相等，请你返回-1。
 */
public class No2541 {
    public static void main(String[] args) {
        // 先判断能不能转换  两个数组每一个数之差都是k的整数倍才能转换

        // 若能转换，分步走几步转换   直接相等的不管，一倍k的先转换，然后递增


    }

    static long minOperations(int[] nums1, int[] nums2, int k) {
        // 先判断能不能转换  两个数组每一个数之差都是k的整数倍才能转换
        for (int i = 0; i < nums1.length; i++) {
            if((nums1[i] - nums2[2]) % k != 0 ){
                return -1;
            }
        }
        // 若能转换，分步走几步转换   直接相等的不管，一倍k的先转换，然后递增


        return 0;
    }
}
