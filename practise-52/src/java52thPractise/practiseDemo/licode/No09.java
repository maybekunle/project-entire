package java52thPractise.practiseDemo.licode;

/**
 * 验证是不是回文数
 */
public class No09 {
    public static void main(String[] args) {
        boolean s = isPalindrome(34543);
        System.out.println(s);
    }

    static boolean isPalindrome(int x) {
        // String s = Integer.toString(x);
        // for (int i = 0, j = s.length() - 1; i < s.length(); i++, j--) {
        //     if (i <= j) {
        //         if (s.charAt(i) != s.charAt(j)) {
        //             return false;
        //         }
        //     }
        // }
        // return true;


        // String s = Integer.toString(x);
        // StringBuffer s2 = new StringBuffer(s);
        // String s3 = s2.reverse().toString();
        // if(s.equals(s3)){
        //     return true;
        // }else {
        //     return false;
        // }
        if(x < 0){
            return false;
        }
        int temp = x;
        int reverse = 0;
        while (x != 0){
            reverse = reverse*10 + x%10;
            x = x/10;
        }
        return temp == reverse;
    }
}
