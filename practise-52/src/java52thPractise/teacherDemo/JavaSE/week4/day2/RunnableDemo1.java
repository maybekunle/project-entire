package java52thPractise.teacherDemo.JavaSE.week4.day2;

public class RunnableDemo1 {
    public static void main(String[] args) {
        SellWindow sellWindow = new SellWindow();
        Thread t1 = new Thread(sellWindow);
        Thread t2 = new Thread(sellWindow);
        Thread t3 = new Thread(sellWindow);
        t1.setName("光头强");
        t2.setName("灰太狼");
        t3.setName("派大星");
        t1.start();
        t2.start();
        t3.start();
    }
}

class SellWindow implements Runnable{

    int tickets = 100;

    @Override
    public void run() {
        while (true) {
            if (tickets > 0) {
                tickets--;
                // 咋没法用getname了？？？？？？？？？？？？？
                System.out.println(Thread.currentThread().getName() + "卖出了第： " + tickets + " 张票！");
            }
        }
    }
}