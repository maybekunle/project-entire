package java52thPractise.teacherDemo.JavaSE.week4.day2;

public class SyncDemo2 {
    public static void main(String[] args) {
        SellWindow2 sellWindow = new SellWindow2();

        Thread t1 = new Thread(sellWindow);
        Thread t2 = new Thread(sellWindow);
        Thread t3 = new Thread(sellWindow);

        t1.setName("光头强");
        t2.setName("灰太狼");
        t3.setName("派大星");

        t1.start();
        t2.start();
        t3.start();
    }
}

class SellWindow2 implements Runnable{

    int tickets = 100;

    A a = new A();
    String s = new String();
    Integer c = 10;
    @Override
    public void run() {
        while (true) {
            synchronized (c) {
                if (tickets > 0) {
                    tickets--;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // 咋没法用getname了？？？？？？？？？？？？？
                    System.out.println(Thread.currentThread().getName() + "卖出了第： " + tickets + " 张票！");
                }else{
                    break;
                }
            }
        }
    }
}

class A{}