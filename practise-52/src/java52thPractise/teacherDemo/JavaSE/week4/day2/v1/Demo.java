package java52thPractise.teacherDemo.JavaSE.week4.day2.v1;

public class Demo {
    public static void main(String[] args) {
        // 创建蒸笼对象
        Box box = new Box();

        // 创建生产者任务
        ProducerTask producerTask = new ProducerTask(box);

        // 创建消费者任务
        ConsumerTask consumerTask = new ConsumerTask(box);

        // 创建生产者线程
        Thread t1 = new Thread(producerTask);
        t1.setName("生产者");
        t1.start();  // 忘启动死锁了

        // 创建消费者线程
        Thread t2 = new Thread(consumerTask);
        t2.setName("消费者");
        t2.start();

    }
}
