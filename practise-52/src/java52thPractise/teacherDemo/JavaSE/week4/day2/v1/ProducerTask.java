package java52thPractise.teacherDemo.JavaSE.week4.day2.v1;

import java.util.Random;

// 生产者
public class ProducerTask implements Runnable{

    Box box;
    Food[] foods = {
            new Food("熊猫肉",66),
            new Food("天鹅肉",55),
            new Food("蛤蟆肉",22),
    };

    Random random = new Random();

    public ProducerTask(Box box) {
        this.box = box;
    }

    @Override
    public void run() {
        // 生产包子
        while (true){
            synchronized (box){
                // 判断蒸笼状态
                if(box.isEmpty()){
                    // 没有包子，生产包子
                    box.setFood(foods[random.nextInt(foods.length)]);
                    // 通知消费者吃
                    box.notify();
                }else {
                    // 如果蒸笼非空，有包子，不生产
                    // 阻止自己生产 wait
                    try {
                        box.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
