package java52thPractise.teacherDemo.JavaSE.week4.day2.v1;


// 定义蒸笼类
public class Box {
    // 定义成员变量
    Food food;

    // 定义方法
    // 生产包子的方法 只有生产者才执行
    public  void setFood(Food newFood){
        food = newFood;
        System.out.println(Thread.currentThread().getName() + "生产了： " + food);
    }

    // 吃包子的方法
    public void eatFood(){
        System.out.println(Thread.currentThread().getName() + "吃了： " + food);
        food = null;
    }

    // 判断蒸笼状态的方法
    public boolean isEmpty(){
        return food == null;
    }

}
