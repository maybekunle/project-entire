package java52thPractise.teacherDemo.JavaSE.week4.day2.v1;

// 消费者
public class ConsumerTask implements Runnable{

    // 成员变量
    Box box;

    public ConsumerTask(Box box) {
        this.box = box;
    }

    @Override
    public void run() {
        // 吃包子
        while (true){
            synchronized (box){
                // 判断蒸笼状态

                if(box.isEmpty()){
                    // 如果蒸笼为空
                    // 没有包子，阻止自己吃包子
                    // wait
                    try {
                        box.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else {
                    // 蒸笼非空，有包子
                    // 消费者吃包子通知生产者再生产
                    box.eatFood();
                    box.notify();
                }

            }
        }
    }
}
