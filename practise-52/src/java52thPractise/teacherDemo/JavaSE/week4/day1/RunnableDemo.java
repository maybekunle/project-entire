package java52thPractise.teacherDemo.JavaSE.week4.day1;

/**
 * 为什么Runnable中的run方法会运行在子线程中????啥意思
 */

public class RunnableDemo {
    public static void main(String[] args) {
        RunnableThreadDemo demo = new RunnableThreadDemo();
        Thread thread = new Thread(demo);
        thread.start();

        ThreadDemo2 demo2 = new ThreadDemo2();
        demo2.start();
    }
}

class RunnableThreadDemo implements Runnable{

    @Override
    public void run() {
        System.out.println("hello fuck world !");
    }
}

class ThreadDemo2 extends Thread{

    @Override
    public void run(){
        System.out.println("让子弹飞一会 !");
    }

}