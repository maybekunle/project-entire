package java52thPractise.teacherDemo.JavaSE.week4.day1;

public class Demo4 {
    public static void main(String[] args) {

        TheadYield t1 = new TheadYield("A");
        TheadYield t2 = new TheadYield("B");

        t1.start();
        t2.start();

    }
}

class TheadYield extends Thread{
    public TheadYield(String name) {
        super(name);
    }

    @Override
    public void run(){
        for (int i = 0; i < 10; i++) {
            System.out.println(getName() + "--------" + i);
            Thread.yield();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}