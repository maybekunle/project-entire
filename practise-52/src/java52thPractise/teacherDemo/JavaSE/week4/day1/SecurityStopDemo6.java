package java52thPractise.teacherDemo.JavaSE.week4.day1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SecurityStopDemo6 {
    public static void main(String[] args) {

        ThreadDemo td = new ThreadDemo();
        td.start();

        for (int i = 0; i < 3; i++) {
            System.out.println("main ---- " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 中断子线程
        td.flag = false;

    }
}

class ThreadDemo extends Thread {
    boolean flag = true;

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            if (flag) {
                System.out.println(getName() + " ----- " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                // 发生了中断
                FileWriter writer = null;
                try {
                    writer = new FileWriter(new File("d://3//log.txt"));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date = sdf.format(new Date());
                    writer.write(date + getName() + " 发生了中断！");
                    writer.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    }
}