package java52thPractise.teacherDemo.JavaSE.week4.day1;

public class YieldDemo {
    public static void main(String[] args) {
        new ThreadYield("A").start();
        new ThreadYield("B").start();
    }
}

class ThreadYield extends Thread{
    public ThreadYield(String name) {
        super(name);
    }

    @Override
    public void run(){
        for (int i = 0; i < 10; i++) {
            System.out.println(getName() + "------" + i);
        }

        Thread.yield();
    }
}