package java52thPractise.teacherDemo.JavaSE.week4.day1;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Demo2 {
    public static boolean flag = true;

    public static void main(String[] args) {
        System.out.println("main before");
        System.out.println("hello before");
        sayHello();
        System.out.println("hello after");

        System.out.println("wait before");
        waitToStop();
        System.out.println("wait after");
        System.out.println("main after");

    }

    private static void waitToStop() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (flag) {
                    String s = scanner.nextLine();
                    if (s.equals("gun")) {
                        flag = false;
                        break;
                    }
                }
            }
        }).start();

    }

    private static void sayHello() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (flag) {
                    System.out.println("hello fuck world");
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
