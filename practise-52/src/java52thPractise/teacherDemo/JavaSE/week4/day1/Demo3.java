package java52thPractise.teacherDemo.JavaSE.week4.day1;

import java.util.concurrent.TimeUnit;

public class Demo3 {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("main before");
        MyThread1 t1 = new MyThread1();
        MyThread1 t3 = new MyThread1();
        t1.setName("徐凤年");
        t3.setName("光头强");
        t1.start();
        t3.start();
        // join 也不好使啊？？？？？？？？？？？？？
       // t3.join();
        MyThread2 t2 = new MyThread2();
        t2.start();
        t2.setName("灰太狼");
        t2.join();

        System.out.println("main after");
    }
}

class MyThread1 extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(getName() + "hello fuck world " + i + " times!");
        }

    }
}


class MyThread2 extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(getName() + "拍了怕第 " + i + " 小肚子");
        }

    }
}