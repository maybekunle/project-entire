package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v4;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID = -123456789L;
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
