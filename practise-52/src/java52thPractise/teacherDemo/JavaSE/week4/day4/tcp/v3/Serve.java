package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v3;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Serve {
    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(12138);
        ExecutorService pool = Executors.newCachedThreadPool();
       pool.submit(new ConnectTask(socket));
       pool.submit(new ConnectTask(socket));
       pool.submit(new ConnectTask(socket));
    }
}

class ConnectTask implements Runnable{

    ServerSocket serverSocket;

    public ConnectTask(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        Socket socket = null;
        InputStream inputStream = null;
        try {
           socket = serverSocket.accept();
            inputStream = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = new byte[1024];
        int index;
        while (true) {
            try {
                if (((index = inputStream.read(bytes)) == -1)) break;
                System.out.println("收到了来自："+ Thread.currentThread().getName() + socket.getInetAddress()
                        +"的消息：-->" + new String(bytes,0,index));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
