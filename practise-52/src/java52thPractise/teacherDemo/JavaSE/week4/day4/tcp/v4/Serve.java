package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v4;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serve {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerSocket socket = new ServerSocket(12138);
        Socket socket1 = socket.accept();
        InputStream inputStream = socket1.getInputStream();
        ObjectInputStream in = new ObjectInputStream(inputStream);
        Object o = in.readObject();
        System.out.println(o);
        in.close();
        socket.close();

    }
}
