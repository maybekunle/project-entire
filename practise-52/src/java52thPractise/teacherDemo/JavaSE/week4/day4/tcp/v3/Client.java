package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v3;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 12138);
        OutputStream outputStream = socket.getOutputStream();
        Scanner scanner = new Scanner(System.in);
        while (true){
            String s = scanner.nextLine();
            outputStream.write(s.getBytes());
        }

        // 怎么关不掉？？？
        // outputStream.close();
    }
}
