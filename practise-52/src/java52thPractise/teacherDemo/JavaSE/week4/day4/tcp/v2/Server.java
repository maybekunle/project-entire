package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        // 1. 创建服务端的socket对象(ServerSocket)
        ServerSocket serverSocket = new ServerSocket(12138);
        // 2. 通过accept建立连接, 得到socket对象
        Socket accept = serverSocket.accept();
        // 3. 从socket中得到输入输出流
        InputStream inputStream = accept.getInputStream();
        FileWriter writer = new FileWriter("d://1//cxc//tcp2.txt");
        // 4. 利用输入输出流进行读写操作
        int index;
        String s = "";
        byte[] bytes = new byte[1024];
        while ((index = inputStream.read(bytes)) != -1){
            // s += inputStream.read(bytes,0,index);
            // System.out.println(new String(bytes,0,index));
            writer.write(new String(bytes));
        }

        writer.flush();
        writer.close();

        // 5. 释放资源
        inputStream.close();
        serverSocket.close();
    }
}
