package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v4;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 12138);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream obj = new ObjectOutputStream(outputStream);
        Student student = new Student("猪猪侠", 16);
        obj.writeObject(student);
        obj.close();

    }
}
