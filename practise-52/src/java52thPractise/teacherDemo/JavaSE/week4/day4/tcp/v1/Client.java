package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        // 1. 创建客户端Socket对象
        Socket socket = new Socket("127.0.0.1", 12138);
        // 2. 从socket中获取输入输出流
        OutputStream outputStream = socket.getOutputStream();
        // 3. 利用输出输出流进行读写操作
        outputStream.write("减肥必胜".getBytes());
        // 4. 释放资源close
        outputStream.close();
    }
}
