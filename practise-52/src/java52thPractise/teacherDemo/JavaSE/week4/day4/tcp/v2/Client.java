package java52thPractise.teacherDemo.JavaSE.week4.day4.tcp.v2;

import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        // 1. 创建客户端Socket对象
        Socket socket = new Socket("127.0.0.1", 12138);
        // 2. 从socket中获取输入输出流
        OutputStream outputStream = socket.getOutputStream();

        FileReader reader = new FileReader("D://1//cxc//a.txt");
        char[] chars = new char[1024];
        int index;
        while ((index = reader.read(chars)) != -1){
            System.out.println(chars);
            // 3. 利用输出输出流进行读写操作
            outputStream.write((new String(chars)).getBytes());
        }

        // 4. 释放资源close
        outputStream.close();
    }
}
