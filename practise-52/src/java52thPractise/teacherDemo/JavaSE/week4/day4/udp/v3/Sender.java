package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v3;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Scanner;

public class Sender {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(9527);
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("输入对话：");
            String s = scanner.nextLine();

            DatagramPacket sendPacket = NetworkUtils.getSendPacket(s, "127.0.0.1", 12138);
            socket.send(sendPacket);

            DatagramPacket receivePacket = NetworkUtils.getReceivePacket();
            socket.receive(receivePacket);

            String msg = NetworkUtils.parseMsg(receivePacket);
            System.out.println("收到了来自：" + receivePacket.getAddress() + "的消息--> " + msg);
        }

    }
}
