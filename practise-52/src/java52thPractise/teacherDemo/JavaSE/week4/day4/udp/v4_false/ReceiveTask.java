package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4_false;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class ReceiveTask implements Runnable{
    @Override
    public void run() {
        DatagramSocket receiveSocket = null;
        try {
             receiveSocket = new DatagramSocket(8888);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        DatagramPacket receivePacket = NetworkUtils.getReceivePacket();
        try {
            receiveSocket.receive(receivePacket);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String s = NetworkUtils.parseMsg(receivePacket);

        System.out.println("来自" + receivePacket.getAddress() + "的消息： " + s);
    }
}
