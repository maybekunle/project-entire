package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkUtils {

    // 获取发送的数据包
    public static DatagramPacket getSendPacket(String msg,String ip,int port) throws UnknownHostException {

        // 把发送信息打包
        byte[] bytes = msg.getBytes();
        InetAddress inetAddress = InetAddress.getByName(ip);
        DatagramPacket packet = new DatagramPacket(bytes, 0, bytes.length, inetAddress, port);

        return packet;
    }

    // 获取接受的数据包
    public static DatagramPacket getReceivePacket(){
        // 接收数据包
        byte[] bytes = new byte[1024];
        DatagramPacket packet = new DatagramPacket(bytes, 0, bytes.length);

        return packet;
    }

    // 解析数据包
    public static String parseMsg(DatagramPacket packet){

        byte[] data = packet.getData();
        int offset = packet.getOffset();
        int length = packet.getLength();

        InetAddress address = packet.getAddress();
       // String name = address.toString();

        String s = new String(data,offset,length);
     //   return name + ":" + s;
        return  ":" + s;
    }

}
