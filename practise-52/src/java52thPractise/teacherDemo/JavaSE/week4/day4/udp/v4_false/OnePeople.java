package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4_false;

public class OnePeople {
    public static void main(String[] args) {
        SenderTask senderTask = new SenderTask();
        Thread t1 = new Thread(senderTask);
        t1.start();

        ReceiveTask receiveTask = new ReceiveTask();
        Thread t2 = new Thread(receiveTask);
        t2.start();
    }
}
