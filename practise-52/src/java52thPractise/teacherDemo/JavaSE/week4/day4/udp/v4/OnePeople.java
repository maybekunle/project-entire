package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4;

import java.net.DatagramSocket;
import java.net.SocketException;

public class OnePeople {
    public static void main(String[] args) throws SocketException {
        DatagramSocket datagramSocket = new DatagramSocket(9999);
        SendTask sendTask = new SendTask(datagramSocket, "127.0.0.1", 8888);
        Thread send = new Thread(sendTask);
        send.start();

        ReceiveTask receiveTask = new ReceiveTask(datagramSocket);
        Thread receive = new Thread(receiveTask);
        receive.start();

    }
}
