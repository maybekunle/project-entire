package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v1;

import java.io.IOException;
import java.net.*;

public class Sender {
    public static void main(String[] args) throws IOException {
        // 创建发送端socket对象
        DatagramSocket datagramSocket = new DatagramSocket(8888);

        // 把要发送的数据打包
        String s = "中午吃嘛？";
        byte[] bytes = s.getBytes();
        // 指定ip地址
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
        // 指定端口号
        int port = 9999;
        DatagramPacket packet = new DatagramPacket(bytes,0,bytes.length,inetAddress,port);

        // 把数据发出去
        datagramSocket.send(packet);

        // 释放资源
        datagramSocket.close();
    }
}
