package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SendTask implements Runnable{
    DatagramSocket datagramSocket;
    String ip;
    int port;

    public SendTask(DatagramSocket datagramSocket, String ip, int port) {
        this.datagramSocket = datagramSocket;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("请说话：");
            String s = scanner.nextLine();
            DatagramPacket sendPacket = null;
            try {
                 sendPacket = NetworkUtils.getSendPacket(s, ip, port);
                datagramSocket.send(sendPacket);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
