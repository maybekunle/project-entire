package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v2;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Receiver {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket(8888);
        DatagramPacket receivePacket = NetworkUtils.getReceivePacket();

        datagramSocket.receive(receivePacket);

        String s = NetworkUtils.parseMsg(receivePacket);
        System.out.println(s);

        datagramSocket.close();
    }
}
