package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v2;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Sender {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket(9999);

        String s = "吃饱了，有点撑";
        DatagramPacket sendPacket = NetworkUtils.getSendPacket(s, "127.0.0.1", 8888);
        datagramSocket.send(sendPacket);
        datagramSocket.close();
    }
}
