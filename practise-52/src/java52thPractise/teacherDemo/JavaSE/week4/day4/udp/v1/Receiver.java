package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Receiver {
    public static void main(String[] args) throws IOException {
        // 创建接收端socket对象
        DatagramSocket datagramSocket = new DatagramSocket(9999);

        // 创建接收包
        byte[] bytes = new byte[1024];
        DatagramPacket packet = new DatagramPacket(bytes, 0, bytes.length);

        // 接受数据
        datagramSocket.receive(packet);

        // 解析数据
        byte[] data = packet.getData();
        int offset = packet.getOffset();
        int length = packet.getLength();

        String s = new String(data,offset,length);
        System.out.println("接受到了来自" + packet.getAddress() + s);
    }
}
