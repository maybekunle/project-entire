package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4_false;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class SenderTask implements Runnable {

    @Override
    public void run() {
        DatagramSocket sendSocket = null;
        try {
            sendSocket = new DatagramSocket(9999);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String ip = "127.0.0.1";
        int port = 8888;
        DatagramPacket sendPacket = null;
        try {
            sendPacket = NetworkUtils.getSendPacket(s, ip, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            sendSocket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
