package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4_false;

public class AnotherPeople {
    public static void main(String[] args) {
        ReceiveTask receiveTask = new ReceiveTask();
        Thread t2 = new Thread(receiveTask);
        t2.start();

        SenderTask senderTask = new SenderTask();
        Thread t1 = new Thread(senderTask);
        t1.start();
    }
}
