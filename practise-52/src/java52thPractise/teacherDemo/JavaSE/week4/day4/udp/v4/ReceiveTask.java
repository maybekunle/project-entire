package java52thPractise.teacherDemo.JavaSE.week4.day4.udp.v4;

import java52thPractise.teacherDemo.JavaSE.week4.day4.udp.utiles.NetworkUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ReceiveTask implements Runnable{
    DatagramSocket datagramSocket;

    public ReceiveTask(DatagramSocket datagramSocket) {
        this.datagramSocket = datagramSocket;
    }

    @Override
    public void run() {
        DatagramPacket receivePacket = NetworkUtils.getReceivePacket();
        while (true) {
            try {
                datagramSocket.receive(receivePacket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String s = NetworkUtils.parseMsg(receivePacket);
            System.out.println("来自： " + receivePacket.getAddress() + "的消息： --> " + s);
        }
    }
}
