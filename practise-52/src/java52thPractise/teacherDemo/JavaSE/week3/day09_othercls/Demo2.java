package java52thPractise.teacherDemo.JavaSE.week3.day09_othercls;

public class Demo2 {
    public static void main(String[] args) {
        Object obj = true ? new Integer(1):new Double(2.0);
        System.out.println(obj);

        Integer i1 = new Integer(125);
        Integer i2 = new Integer(125);
        System.out.println(i1 == i2);
// false
        Integer i3 = new Integer(128);
        Integer i4 = new Integer(128);
        System.out.println(i3 == i4);
// false 对象地址不一样

    }
}
