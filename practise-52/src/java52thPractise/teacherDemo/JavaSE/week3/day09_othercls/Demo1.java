package java52thPractise.teacherDemo.JavaSE.week3.day09_othercls;

public class Demo1 {
    static int end = Integer.MAX_VALUE;
    static int start = end - 5;

    public static void main(String[] args) {
        int count = 0;
        for (int i = start; i <= end; i++){
            count++;
        }
        System.out.println(count);
    }
}
