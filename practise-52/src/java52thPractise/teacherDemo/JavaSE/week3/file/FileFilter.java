package java52thPractise.teacherDemo.JavaSE.week3.file;

import java.io.File;
import java.util.Arrays;

public class FileFilter {
    public static void main(String[] args) {
        File file = new File("d://1");
        MyFileFilter filter = new MyFileFilter();
        // File[] files = file.listFiles(new MyFileFilter());
        File[] files = file.listFiles(filter);
        System.out.println(Arrays.toString(files));
    }
}

class MyFileFilter implements java.io.FileFilter {

    @Override
    public boolean accept(File pathname) {

        return pathname.getName().endsWith("txt");
    }
}