package java52thPractise.homeworkDemo.JavaSE.week3.day6;

import java.io.*;

public class work2 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        serialize();

        unSerialize();

    }

    static void unSerialize() throws IOException, ClassNotFoundException {

        // 创建输入流对象
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("d://2//work2.txt"));
        Object o = inputStream.readObject();
        System.out.println(o);

        inputStream.close();
    }
    static void serialize() throws IOException {
        // 创建学生对象
        User user = new User(1,"李一","8008820",new Address("河北","062150"));

        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("d://2//work2.txt"));
        outputStream.writeObject(user);

        outputStream.close();
    }

}
class User implements Serializable{
    int id;
    String name;
    String mobile;
    Address address;

    public User() {
    }

    public User(int id, String name, String mobile, Address address) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", mobile='").append(mobile).append('\'');
        sb.append(", address=").append(address);
        sb.append('}');
        return sb.toString();
    }
}

class Address implements Serializable{
    String detail;
    String code;

    public Address(String detail, String code) {
        this.detail = detail;
        this.code = code;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("detail='").append(detail).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

