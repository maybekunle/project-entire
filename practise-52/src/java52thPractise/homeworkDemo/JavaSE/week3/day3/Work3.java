package java52thPractise.homeworkDemo.JavaSE.week3.day3;

import java.util.Scanner;

public class Work3 {
    public static void main(String[] args) {
        // 获取一个字符串
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入一个字符串：");
        String s = scanner.nextLine();
        boolean q = judgeSymmetry(s);
        System.out.println(q);
    }

    private static boolean judgeSymmetry(String s) {

        StringBuffer s2 = new StringBuffer(s);
        String s3 = s2.reverse().toString();
        if(s.equals(s3)){
            return true;
        }else {
            return false;
        }
    }
}
