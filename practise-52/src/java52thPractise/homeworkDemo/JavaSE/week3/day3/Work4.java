package java52thPractise.homeworkDemo.JavaSE.week3.day3;

import java.util.Random;

public class Work4 {
    public static void main(String[] args) {
        // 创建长度是100 的字符数组
        String[] str = new String[100];
        // 随机生成长度是i 的字符串
        String s = getAString(2);
        // 初始化数组
        randomCreat(str);
        for (int i = 0; i < str.length; i++) {
            System.out.print(str[i] + " ");
            if(i%10 == 9){
                System.out.println();
            }
        }

        for (int i = 0; i < str.length - 1; i++) {
            for (int j = i+1; j < str.length; j++) {
                int count = 1;
                if(str[i].equalsIgnoreCase(str[j])){
                    count++;
                }
                if(j == str.length - 1 && count != 1){
                    System.out.println(str[i] + "发生了重复 重复了" + count + "次");
                }
            }
        }
    }

    private static String getAString(int j) {
        // 字符池
        String s1 = "";
        String s2 = "";
        for(int i = 0;i<=9;i++){
            s1 += i;
        }
        for(char i = 'a';i<='z';i++){
            s1 += i;
        }
        for(char i = 'A';i<='Z';i++){
            s1 += i;
        }

        for (int i = 0; i < j; i++) {
            Random random = new Random();
            int index = random.nextInt(s1.length());
            s2 += s1.charAt(index);
        }
        return s2;
    }

    private static void randomCreat(String[] str) {
        for (int i = 0; i < str.length; i++) {
            str[i] = getAString(2);
        }
    }
}
