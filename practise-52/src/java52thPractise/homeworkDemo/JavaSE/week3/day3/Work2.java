package java52thPractise.homeworkDemo.JavaSE.week3.day3;

import java.util.Arrays;
import java.util.Comparator;

public class Work2 {
    public static void main(String[] args) {
        Cat[] cats = {
                new Cat(2,"英短",1200),
                new Cat(1,"美短",1500),
                new Cat(3,"加菲猫",1300),
                new Cat(2,"中华田园猫",1000),
                new Cat(2,"金渐层",2000),
        };
        // 实现comparable接口
        System.out.println("排序前：");
        System.out.println(Arrays.toString(cats));
        System.out.println("按年龄排序：");
        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));
// ======================================================================
        Cat2[] cat2s = {
                new Cat2(2,"英短",1200),
                new Cat2(1,"美短",1500),
                new Cat2(3,"加菲猫",1300),
                new Cat2(2,"中华田园猫",1000),
                new Cat2(2,"金渐层",2000),
        };
        // 实现comparator接口

        // 匿名内部类方法实现
        System.out.println("排序前：");
        System.out.println(Arrays.toString(cat2s));
        Arrays.sort(cat2s, new Comparator<Cat2>() {
            @Override
            public int compare(Cat2 o1, Cat2 o2) {
                return (int) (o2.price - o1.price);
            }
        });
        System.out.println("按价钱排序后：");
        System.out.println(Arrays.toString(cat2s));

        Arrays.sort(cat2s, new Comparator<Cat2>() {
            @Override
            public int compare(Cat2 o1, Cat2 o2) {
                return o2.name.length() - o1.name.length();
            }
        });
        System.out.println("按名字长短排序后：");
        System.out.println(Arrays.toString(cat2s));


        // 使用lambda表达式
        Arrays.sort(cat2s,(cat1,cat2) -> (int) (cat2.price - cat1.price));
        System.out.println("按价钱排序后：");
        System.out.println(Arrays.toString(cat2s));

        Arrays.sort(cat2s,(cat1,cat2) -> cat2.name.length() - cat1.name.length());
        System.out.println("按名字排序后：");
        System.out.println(Arrays.toString(cat2s));

        // 综合排序

    }

}

class Cat implements Comparable<Cat>{
    int age;
    String name;
    double price;

    public Cat(int age, String name, double price) {
        this.age = age;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Cat{");
        sb.append("age=").append(age);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Cat o) {
        return this.age - o.age;
    }

}


//============================
class Cat2  {
    int age;
    String name;
    double price;

    public Cat2(int age, String name, double price) {
        this.age = age;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Cat2{");
        sb.append("age=").append(age);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

}