package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.io.File;
import java.util.Arrays;

/**
 * 带过滤器的listFiles方法练习
 * 自己做一个目录，里面放些一下文件，文件夹 用带过滤器的listFiles方法进行File对象的筛选
 * 条件一：仅留下文件夹名包括a的文件夹
 * 条件二：仅留下.txt结尾的文本文件
 * 条件三：仅留下以a开头的文件
 * 要求:使用Lambda表达式
 * 无需考虑多级目录,仅考虑一级目录即可！
 */
public class Work5 {
    public static void main(String[] args) {

        File file = new File("d://1");

        // 留下 .txt 结尾的文件
        File[] files1 = file.listFiles((pathname) -> pathname.getName().endsWith(".txt"));

        // 留下 a开头的文件
        File[] files2 = file.listFiles((pathname) -> pathname.getName().startsWith("a"));

        System.out.println(Arrays.toString(files1));
        System.out.println(Arrays.toString(files2));
    }
}
