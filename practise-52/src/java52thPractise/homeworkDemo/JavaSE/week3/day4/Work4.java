package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.io.File;

/**
 * 递归删除目录与文件
 */
public class Work4 {
    public static void main(String[] args) {
        File file = new File("d://1");
        deleteDirectory(file);
    }

    static void deleteDirectory(File file){
        File[] files = file.listFiles();
        for (File file1 : files) {
            if(file1.isFile()){
                file1.delete();
            }else if (file1.isDirectory()){
                deleteDirectory(file1);
            }
        }
        file.delete();
    }
}
