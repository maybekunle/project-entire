package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Work8 {
    public static void main(String[] args) throws IOException {
        String[] words = {"verb:eat","verb:drink","verb:sleep",
                "verb:play","noun:rice","noun:meat","noun:hand","noun:hair"};

        OutputStream out1 = new FileOutputStream("d://1//verb.txt",true);
        OutputStream out2 = new FileOutputStream("d://1//noun.txt",true);
        for (int i = 0; i < words.length; i++) {
            if(words[i].startsWith("verb")){
                String s = words[i];
                byte[] bytes = s.getBytes();
                out1.write(bytes);
                out1.write("\r\n".getBytes());
            }
            else {
                String s = words[i];
                byte[] bytes = s.getBytes();
                out2.write(bytes);
                out2.write("\r\n".getBytes());
            }
        }
        out1.close();
        out2.close();
    }
}
