package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Work6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 输入路径
        System.out.print("输入路径:");
        String path = scanner.nextLine();
        // 输入内容
        System.out.print("输入内容:");
        String s = scanner.nextLine();
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(path);
            byte[] bytes = s.getBytes();
            outputStream.write(bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
