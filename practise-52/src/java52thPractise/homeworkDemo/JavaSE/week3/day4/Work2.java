package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.util.Scanner;

public class Work2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int num = 0;
        boolean flag = true;
        while (flag) {
            System.out.println("输入一个整数：");
            try {
                String s = scanner.nextLine();
                num = Integer.parseInt(s);
                flag = false;
            } catch (Exception e) {
                System.out.println("输入正确的数字");
            }
        }

    }
}
