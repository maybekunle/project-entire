package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class Work7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 输入路径
        System.out.print("输入路径:");
        String path = scanner.nextLine();
        // 输入内容
        String s = "";
        while (!s.equals("end")){
            System.out.print("输入内容:");
            s = scanner.nextLine();

            try (OutputStream outputStream = new FileOutputStream(path,true);){
                byte[] bytes = s.getBytes();
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
