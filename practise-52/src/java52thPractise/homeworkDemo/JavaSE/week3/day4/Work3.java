package java52thPractise.homeworkDemo.JavaSE.week3.day4;

import java.util.Scanner;

public class Work3 {
    public static void main(String[] args) throws MyException1 {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        age1(i);

        age2(i);
    }

    static void age1(int x) throws MyException1 {
        if(x<0 || x>130){
            throw new MyException1("年龄异常！");
        }
    }

    static void age2(int x)  {
        if(x<0 || x>130){
            throw new MyException2("年龄异常！");
        }
    }

}
// 编译时异常
class MyException1 extends Exception{
    public MyException1() {
    }

    public MyException1(String message) {
        super(message);
    }
}

// 运行时异常
class MyException2 extends RuntimeException{
    public MyException2() {
    }

    public MyException2(String message) {
        super(message);
    }
}