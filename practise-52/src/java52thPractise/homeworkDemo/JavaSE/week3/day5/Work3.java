package java52thPractise.homeworkDemo.JavaSE.week3.day5;

import java.io.*;
import java.util.Arrays;

public class Work3 {
    public static void main(String[] args) throws IOException {
        File file = new File("d://1//work3.txt");

        // 字节流
        FileInputStream inputStream = new FileInputStream(file);
        FileOutputStream outputStream = new FileOutputStream(file,true);

        String s = "";
        while (true){
            int read = inputStream.read();
            if(read == -1){
                break;
            }else {
                s += (char)read;  // stringBuffer
            }
        }
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        String s2 = new String(chars);

        byte[] bytes = s2.getBytes();
        outputStream.write("\r\n".getBytes());
        outputStream.write(bytes);

        inputStream.close();
        outputStream.close();
    }
}
