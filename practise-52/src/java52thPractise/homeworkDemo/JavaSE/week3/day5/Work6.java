package java52thPractise.homeworkDemo.JavaSE.week3.day5;

import java.io.*;

public class Work6 {
    public static void main(String[] args) throws IOException {
        File file1 = new File("d://1//work6.txt");
        File file2 = new File("d://1//work6-2.txt");

        encodeFile(file1,file2);

    }

    private static void encodeFile(File file1, File file2) throws IOException {
        FileReader reader = new FileReader(file1);
        FileWriter writer = new FileWriter(file2);

        String s = "";
        // char[] chars = new char[1];
        // char[] chars = new char[1024];
        int index = 0;
        // while ((index = reader.read(chars)) != -1){
        //     s += (char)reader.read(chars);
        // }
        while (reader.read() != -1){
            s += (char)reader.read();
        }
        System.out.println(s);
        for (int i = 0; i < s.length(); i++) {
            System.out.print(s.charAt(i)+ " ");
            // if(s.charAt(i) <'9' && s.charAt(i)>= '0'){
            //     s.charAt(i) = (char) (s.charAt(i) + 1);
            // }
        }
    }
}
