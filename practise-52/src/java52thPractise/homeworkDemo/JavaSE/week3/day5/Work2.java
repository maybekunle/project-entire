package java52thPractise.homeworkDemo.JavaSE.week3.day5;

import java.io.*;

public class Work2 {
    public static void main(String[] args) throws IOException {
        File file = new File("d://1//work2.txt");

        int even = 0;
        int odd = 0;

        FileOutputStream outputStream = new FileOutputStream(file,true);
        FileInputStream inputStream = new FileInputStream(file);

        // int read = inputStream.read();
        // while (read != -1){
        //     if(read % 2 == 0){
        //         even++;
        //     }else {
        //         odd++;
        //     }
        // }

        while (true){
            int readData = inputStream.read();
            if (readData == -1) {
                break;
            }else if (readData % 2 == 0){
                even++;
            }else {
                odd++;
            }
        }

        inputStream.close();
        String s1 = "偶数个数："+even + "个";
        String s2 = "偶数个数："+odd + "个";

        byte[] bytes1 = s1.getBytes();
        outputStream.write("\r\n".getBytes());
        outputStream.write(bytes1);
        outputStream.write("\r\n".getBytes());
        byte[] bytes2 = s2.getBytes();
        outputStream.write(bytes2);
        outputStream.close();

    }
}
