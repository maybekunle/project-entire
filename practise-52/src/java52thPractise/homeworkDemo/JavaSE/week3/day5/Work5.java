package java52thPractise.homeworkDemo.JavaSE.week3.day5;

import java.io.*;

public class Work5 {
    public static void main(String[] args) throws IOException {
        File file1 = new File("d://1//work5.txt");
        File file2 = new File("d://1//work5-2.txt");

        FileReader reader = new FileReader(file1);
        FileWriter writer = new FileWriter(file2);
        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        while ((line = bufferedReader.readLine()) != null){
            StringBuffer stringBuffer = new StringBuffer(line);
            String s = stringBuffer.reverse().toString();
            writer.write(s);
            writer.write("\t\n");
        }

        writer.flush();
        reader.close();
        writer.close();

    }
}
