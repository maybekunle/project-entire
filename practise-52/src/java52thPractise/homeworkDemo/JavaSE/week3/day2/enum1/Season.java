package java52thPractise.homeworkDemo.JavaSE.week3.day2.enum1;

 enum Season {

    //定义季节枚举类型
    SPRING,
    SUMMER,
    AUTUMN,
    WINTER;
}
