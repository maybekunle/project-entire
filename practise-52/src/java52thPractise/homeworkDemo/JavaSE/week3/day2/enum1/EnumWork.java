package java52thPractise.homeworkDemo.JavaSE.week3.day2.enum1;

public class EnumWork {
    public static void main(String[] args) {

        judgeSeason(Season.SPRING);

    }

    private static void judgeSeason(Season spring) {

        switch (spring){
            case AUTUMN:
                System.out.println("秋天");
                break;
            case WINTER:
                System.out.println("冬天");
                break;
            case SPRING:
                System.out.println("春天");
                break;
            case SUMMER:
                System.out.println("夏天");
                break;
        }

    }

}
