package java52thPractise.homeworkDemo.JavaSE.week3.day2.string;

public class Work3 {

    public static void main(String[] args) {
        String s = "peter piper picked a peck of pickled peppers";
        int count = 0;
        // 先用split 把字符串分割成字符串数组
        String[] str = s.split(" ");
        // 遍历数组
        for (String s1 : str) {
            // 判断每个字符串是否p开头
            if(s1.startsWith("p")){
                count++;
            }
        }
        System.out.println("这段绕口令以p开头的单词有："+count+"个。");
    }
}
