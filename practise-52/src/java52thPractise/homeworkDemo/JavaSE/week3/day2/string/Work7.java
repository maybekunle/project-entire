package java52thPractise.homeworkDemo.JavaSE.week3.day2.string;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Work7 {

    public static void main(String[] args) throws ParseException {

        System.out.println("你来这个倒霉世界已经："+ TimeUtils.bronDays() + "天了。");

    }
}

class TimeUtils{

    // 私有化输入方法
    private static Scanner scanner = new Scanner(System.in);
    // 私有化时间函数
    private TimeUtils(){
    }

    public static int bronDays() throws ParseException {
        // 按格式获取生日信息
        System.out.println("请输入生日（格式为yyyy-MM-dd）：");
        String birthday = scanner.nextLine();

        // 格式化生日信息，获取生日当天时间戳（毫秒数）
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdayTime = simpleDateFormat.parse(birthday);
        long birthSeconds = birthdayTime.getTime();

        // 获取当前日期的时间戳（毫秒数）
        Date currentTime = new Date();
        long currentSeconds = currentTime.getTime();

        // 计算生日时间戳和当前时间戳之差
        long seconds = currentSeconds - birthSeconds;

       if(seconds < 0){
           System.out.println("请输入正确的生日时间");
           return 0;
       }else {
           return (int) (seconds/1000/3600/24);
       }

    }

}