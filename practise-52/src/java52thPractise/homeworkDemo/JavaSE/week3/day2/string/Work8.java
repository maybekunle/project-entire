package java52thPractise.homeworkDemo.JavaSE.week3.day2.string;

import java.util.Arrays;
import java.util.Random;

public class Work8 {
    public static void main(String[] args) {
        // 创建长度为8 的数组
        String[] str = new String[8];
        // 生成长度为5的随机字符串
        String s = getRandomString(5);
        // 初始化字符串数组
        for (int i = 0; i < str.length; i++) {
            str[i] = getRandomString(5);
        }
        System.out.println(Arrays.toString(str));

        // 排序
        sortString(str);

    }

    private static void sortString(String[] str) {
        for (int k = 0; k <str.length;k++) {
            for (int i = 0; i < str.length - 1; i++) {
                int j = str[i].compareToIgnoreCase(str[i + 1]);
                if (j >= 0) {
                    String temp = "";
                    temp = str[i + 1];
                    str[i + 1] = str[i];
                    str[i] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(str));

    }

    private static String getRandomString(int i) {

        // 字符池
        String pool = "";
        // 返回值
        String s = "";
        for(int j = 0;j <=9;j++){
            pool +=j;
        }

        for(char j='a';j <= 'z';j++){
            pool +=j;
        }

        for(char j='A';j <= 'Z';j++){
            pool +=j;
        }

        for (int j = 0; j < i; j++) {
            Random random = new Random();
            int index = random.nextInt(pool.length());
            char c = pool.charAt(index);
            s += c;
        }
        return s;
    }
}
