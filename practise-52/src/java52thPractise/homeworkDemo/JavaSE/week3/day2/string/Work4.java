package java52thPractise.homeworkDemo.JavaSE.week3.day2.string;

public class Work4 {
    public static void main(String[] args) {
        String s = "legendary";

        String s2 = "";
        for (int i = 0; i < s.length(); i++) {
            String s3;
            if (i % 2 == 0) {
                char c = s.charAt(i);
                char c1 = Character.toUpperCase(c);
                s3 = Character.toString(c1);
            } else {
                char c = s.charAt(i);
                char c1 = Character.toLowerCase(c);
                s3 = Character.toString(c1);
            }
            s2 += s3;
        }
        System.out.println(s2);
    }
}
