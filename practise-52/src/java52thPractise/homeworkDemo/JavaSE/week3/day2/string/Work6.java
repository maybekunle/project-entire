package java52thPractise.homeworkDemo.JavaSE.week3.day2.string;

import java.util.Random;

public class Work6 {

    public static void main(String[] args) {

        //获取密码
        String password = getRandomWord(3);
        for (int i = 0; i < 1000; i++) {
            String format = String.format("%03d", i);
            System.out.print(format + " ");
            if(i % 10 == 9){
                System.out.println();
            }
            if (password.equals(format)) {
                System.out.println("生成的密码为： " + format);
                return;
            }
        }
    }

    private static String getRandomWord(int i) {

        // 随机数字符池
        String s = "0123456789";
        String pwd = "";
        for (int j = 0; j < i; j++) {
            // 随机数
            Random random = new Random();
            int index = random.nextInt(s.length());
            char c = s.charAt(index);

            pwd += c;
        }
        return pwd;
    }


}
