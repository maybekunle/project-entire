package java52thPractise.homeworkDemo.JavaSE.test.tset1;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class LoginServer {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerSocket serverSocket = new ServerSocket(12138);
        Socket socket = serverSocket.accept();
        InputStream inputStream = socket.getInputStream();
        ObjectInputStream obj = new ObjectInputStream(inputStream);
        User user = (User) obj.readObject();
        String rMsg = "";
        if(user.username.equals("admin")){
            if(user.password.equals("admin")){
                System.out.println("登陆成功！");
                rMsg = "您已登陆成功！";
            }
        }else {
            System.out.println("登录失败");
            rMsg = "用户名或密码错误！";
        }

        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(rMsg.getBytes());

        obj.close();
        serverSocket.close();
        outputStream.close();
    }
}
