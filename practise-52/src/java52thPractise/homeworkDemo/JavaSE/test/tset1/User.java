package java52thPractise.homeworkDemo.JavaSE.test.tset1;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 2906642554793891381L;

    String username;
        String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("username='").append(username).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
