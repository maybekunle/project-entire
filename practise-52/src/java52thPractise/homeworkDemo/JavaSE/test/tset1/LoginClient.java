package java52thPractise.homeworkDemo.JavaSE.test.tset1;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class LoginClient {
    public static void main(String[] args) throws IOException {
        System.out.println("正在登陆......");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名：");
        String username = scanner.nextLine();
        System.out.println("请输入密码： ");
        String password = scanner.nextLine();

        User user = new User(username,password);

        Socket socket = new Socket("127.0.0.1", 12138);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream obj = new ObjectOutputStream(outputStream);
        obj.writeObject(user);

        InputStream inputStream = socket.getInputStream();
        byte[] bytes = new byte[1024];
        System.out.println(new String(bytes,0, inputStream.read(bytes)));

        obj.close();
        socket.close();
        inputStream.close();
    }
}
