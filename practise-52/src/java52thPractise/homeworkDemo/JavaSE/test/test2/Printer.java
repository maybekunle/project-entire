package java52thPractise.homeworkDemo.JavaSE.test.test2;


class Work {
    public static void main(String[] args) {
        Printer printer = new Printer();
        Number number = new Number(printer);
        Char aChar = new Char(printer);
        number.start();
        aChar.start();
    }
}

class Number extends Thread {
    Printer printer;

    public Number(Printer printer) {
        this.printer = printer;
    }

    @Override
    public void run() {
        for (int i = 1; i < 53; i++) {
            try {
                printer.printInt(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class Char extends Thread {
    Printer printer;

    public Char(Printer printer) {
        this.printer = printer;
    }
    @Override
    public void run() {
        for (char i = 'A'; i <= 'Z'; i++) {
            try {
                printer.printChar(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Printer {
    private int index = 1;

    synchronized void printInt(int i) throws InterruptedException {
        if(index % 3 == 0){
            wait();
        }else {
            System.out.print(i);
        }
        index++;
        notify();
    }

    synchronized void printChar(char c) throws InterruptedException {
        if (index % 3 != 0) {
            wait();
        }
            System.out.print(c);

        index++;
        notify();
    }
}

