package java52thPractise.homeworkDemo.JavaSE.test.test2;

public class Test2 {
    public static void main(String[] args) {
        Printer2 p = new Printer2();
        NumberPrint p1 = new NumberPrint(p);
        CharPrint p2 = new CharPrint(p);
        p1.start();
        p2.start();
    }
}

class Printer2{
    private static int index = 1;
    synchronized void print(int i){
        if (index%3 == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            System.out.print(i);
        index++;
        notify();
    }
    synchronized void printChar(char c){
        if (index%3 != 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            System.out.print(c);
        index++;
        notify();
    }
}

class NumberPrint extends Thread{
    Printer2 p;

    public NumberPrint(Printer2 p) {
        this.p = p;
    }

    @Override
    public void run(){
        for (int i = 1; i < 53; i++) {
            p.print(i);
        }
    }
}

class CharPrint extends Thread{
    Printer2 p;

    public CharPrint(Printer2 p) {
        this.p = p;
    }

    @Override
    public void run(){
        for (char i = 'A'; i <= 'Z'; i++) {
            p.printChar(i);
        }
    }
}