package java52thPractise.homeworkDemo.JavaSE.week4.day3;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Work3 {
    public static void main(String[] args) throws IOException, InterruptedException {

        Timer timer = new Timer();
        MyTask task = new MyTask();
        timer.schedule(task, 3000,5000);
        TimeUnit.SECONDS.sleep(30);
        timer.cancel();

    }
}

class MyTask extends TimerTask{

    FileWriter writer;

    {
        try {
            writer = new FileWriter("d://1//cxc//work3.txt",true);
        } catch (IOException e) {
            System.out.println("1");
        }
    }

    @Override
    public void run() {
        // 获取当前时间
            //把时间写入文件
            try {
                LocalDateTime time = LocalDateTime.now();
                String time2 = time.toString();
                writer.write(time2);
                writer.write("\r\n");
                writer.flush();
             //   writer.close();
            } catch (IOException e) {
                System.out.println("2");
            }
        System.out.println("?");
    }
}