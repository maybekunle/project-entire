package java52thPractise.homeworkDemo.JavaSE.week4.day3;

public class Work4 {
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        Thread t1 = new Thread(thread);
        Thread t2 = new Thread(thread);
        Thread t3 = new Thread(thread);

        t1.start();
        t2.start();
        t3.start();


    }
}

class MyThread implements Runnable {
    static String s = new String();
    static int i = 1;

    @Override
    public void run() {
        synchronized (s) {
            // s.notifyAll();   为什么放这会阻塞？？？？？
            while (i < 76) {
                s.notifyAll();
                System.out.println(Thread.currentThread().getName() + "打印了" + i);
                i++;
                if (i % 5 == 1) {
                    try {
                        s.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            //不结束是因为有线程在阻塞，唤醒就好了
            s.notifyAll();
        }

    }
}