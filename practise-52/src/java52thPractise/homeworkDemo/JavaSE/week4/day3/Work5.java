package java52thPractise.homeworkDemo.JavaSE.week4.day3;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Work5 {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());
        pool.submit(new SleepThread());


    }
}
 class SleepThread extends Thread{
    Random random = new Random();
    @Override
     public void run(){
        int time = random.nextInt(10);
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getName() + "睡眠了： "+ time + "秒");

    }
 }
