package java52thPractise.homeworkDemo.JavaSE.week4.day3;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Work2 {
    public static void main(String[] args) throws IOException {
        FutureTask<String> task1 = new FutureTask<>
                (new CopyThread(new File("d://1//cxc//a.txt"),new File("d://1//cxc//V1.txt")));
        FutureTask<String> task2 = new FutureTask<>
                (new CopyThread(new File("d://1//cxc//a.txt"),new File("d://1//cxc//V2.txt")));
        FutureTask<String> task3 = new FutureTask<>
                (new CopyThread(new File("d://1//cxc//a.txt"),new File("d://1//cxc//V3.txt")));

        Thread t1 = new Thread(task1);
        Thread t2 = new Thread(task2);
        Thread t3 = new Thread(task3);
        t1.start();
        t2.start();
        t3.start();
        String s1 = null;
        String s2 = null;
        String s3 = null;
        try {
            s1 = task1.get();
            s2 = task2.get();
            s3 = task3.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        FileWriter writer = new FileWriter("d://1//cxc//time.txt");
        writer.write(s1 + s2 + s3);
        writer.close();
    }
}

class CopyThread implements Callable<String>{
    File oldFile;
    File newFile;

    public CopyThread(File oldFile, File newFile) {
        this.oldFile = oldFile;
        this.newFile = newFile;
    }

    @Override
    public String call() throws Exception {

        long time1 = System.currentTimeMillis();
        FileReader reader = new FileReader(oldFile);
        FileWriter writer = new FileWriter(newFile);
        char[] chars = new char[1024];
        int index;
        while ((index = reader.read(chars)) != -1){
            writer.write(chars,0,index);
        }
        reader.close();
        writer.close();
        long time2 = System.currentTimeMillis();
        String time = String.valueOf((double) (time2 - time1));

        return "文件复制消耗了：" + time + "毫秒。";
    }
}
