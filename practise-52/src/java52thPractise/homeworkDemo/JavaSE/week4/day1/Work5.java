package java52thPractise.homeworkDemo.JavaSE.week4.day1;

import java.io.*;

public class Work5 {
    public static void main(String[] args) {
        CopyThread2 thread1 = new CopyThread2("d://1//cxc//a.txt","d://2//cxc//a.txt");
        Thread t1 = new Thread(thread1);
        t1.start();
        CopyThread2 thread2 = new CopyThread2("d://1//cxc//b.txt","d://2//cxc//b.txt");
        Thread t2 = new Thread(thread2);
        t2.start();
        CopyThread2 thread3 = new CopyThread2("d://1//cxc//c.txt","d://2//cxc//c.txt");
        Thread t3 = new Thread(thread3);
        t3.start();

    }
}
class CopyThread2 implements Runnable{

    File oldFile;
    File newFile;
    public  CopyThread2 (String oldFile,String newFile){
        this.newFile = new File(newFile);
        this.oldFile = new File(oldFile);
    }

    @Override
    public void run() {
        // File file1 = new File(oldFile);
        // File file2 = new File("d://2//cxc//a.txt");
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            inputStream = new FileInputStream(oldFile);
            outputStream = new FileOutputStream(newFile);
            byte[] bytes = new byte[1024];
            int index;
            // String s = "";
            while ((index = inputStream.read(bytes)) != -1) {
                //s += inputStream.read(bytes, 0, index);
                outputStream.write(bytes,0,index);
                System.out.println(oldFile.getName() + "已经复制了" + (newFile.length() / (double)oldFile.length())*100 + "%" );
            }
            // byte[] bytes1 = s.getBytes();
            // outputStream.write(bytes1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}