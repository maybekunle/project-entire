package java52thPractise.homeworkDemo.JavaSE.week4.day1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Work4 {
    public static void main(String[] args) throws InterruptedException {
        String s = "01234567889";
        String pwd = "";
        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            pwd += s.charAt(random.nextInt(s.length()));
        }
        System.out.println(pwd);

        PwdThread pwdThread = new PwdThread(pwd);
        pwdThread.start();
        try {
            pwdThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

         LogThread logThread = new LogThread(PwdThread.nums);
        logThread.setDaemon(true);
         logThread.start();
         // 在这睡眠是为了让main线程不死,不死的话那么jvm中就还有用户线程,虚拟机就不会退出
        // 等到日志线程执行完了再死
        Thread.sleep(5000);

    }
}

class PwdThread extends Thread {
    static String pwd;
    static String[] nums;
    public PwdThread(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public void run() {
        String num;
         nums = new String[1000];
        for (int i = 0; i < 1000; i++) {
            num = String.format("%3d", i);
            nums[i] = num;
            if (num.equals(pwd)){
                return;
            }
        }

    }

}

class LogThread extends Thread{

    String[] nums;

    public LogThread(String[] nums) {
        this.nums = nums;
    }

    int count = 0;
    String pwd;
    @Override
    public void run(){
        // for (int i = 0; i < nums.length; i++) {
        //     if(nums[i] != null){
        //         count++;
        //     }
        // }
        //
        // String[] real = new String[count];
        // for (int i = 0,j = 0; i < real.length; i++,j++) {
        //     if(nums[j] != null){
        //         real[i] = nums[j];
        //     }else {
        //         j++;
        //     }
        // }
        FileWriter writer = null;
        try {
            writer = new FileWriter(new File("d://3//cxc//log.txt"));
                for (int i = 0; i < nums.length; i++) {
                    if(nums[i] != null){
                        writer.write(nums[i]);
                      //  writer.write("\r\n");
                    }
                }
                writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}