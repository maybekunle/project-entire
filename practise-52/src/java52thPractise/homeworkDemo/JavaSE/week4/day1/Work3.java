package java52thPractise.homeworkDemo.JavaSE.week4.day1;

public class Work3 {
    public static void main(String[] args) throws InterruptedException {
        T1 t1 = new T1();
        T2 t2 = new T2();
        T3 t3 = new T3();

        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
        t2.join();
        t3.start();

    }
}

class T1 extends Thread{

    @Override
    public void run(){
        for (int i = 0; i < 10; i++) {
            System.out.println("光头强：" + " == " + i);
        }
    }

}
class T2 extends Thread{

    @Override
    public void run(){
        for (int i = 0; i < 10; i++) {
            System.out.println("灰太狼：" + " == " + i);
        }
    }

}
class T3 extends Thread{

    @Override
    public void run(){
        for (int i = 0; i < 10; i++) {
            System.out.println("海绵宝宝：" + " == " + i);
        }
    }

}