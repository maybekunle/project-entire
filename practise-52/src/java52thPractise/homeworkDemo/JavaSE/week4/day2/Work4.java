package java52thPractise.homeworkDemo.JavaSE.week4.day2;

public class Work4 {
    public static void main(String[] args) {

        Odd odd = new Odd();
        Even even = new Even();


        odd.start();

        even.start();

    }
}

class Obj {
    static String o = new String();
}

class Odd extends Thread{
    String o = Obj.o;
    @Override
    public void run(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (o) {
                for (int i = 1; i < 100; i += 2) {
                    o.notify();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(i);
                    try {
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

    }
}

class Even extends Thread{
    String o = Obj.o;
    @Override
    public void run(){

            synchronized (o) {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 2; i < 101; i += 2) {
                    o.notify();
                    System.out.println(i);
                    try {
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

    }
}