package java52thPractise.homeworkDemo.JavaSE.week4.day2;

public class Work2 {
    public static void main(String[] args) {
        DownloadThread thread = new DownloadThread();
        Thread thread1 = new Thread(thread);
        Thread thread2 = new Thread(thread);
        Thread thread3 = new Thread(thread);
        thread1.start();
        thread2.start();
        thread3.start();
    }
}

class DownloadThread implements Runnable{
    int num = 100;
    String s = new String();
    @Override
    public void run() {
        synchronized (s) {
            while (true) {
                if (num > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    num--;
                    System.out.println(Thread.currentThread().getName() + "下载了1M，还剩： " + num + "M");
                }else {
                    break;
                }
            }
        }
    }
}