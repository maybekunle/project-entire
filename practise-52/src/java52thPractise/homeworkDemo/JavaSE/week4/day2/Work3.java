package java52thPractise.homeworkDemo.JavaSE.week4.day2;

public class Work3 {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();

        a.start();

        b.start();
    }
}

class D{
    static String s = new String();
}

class A extends Thread{
    D d = new D();
    @Override
    public void run(){
        synchronized (D.s) {
            for (int i = 1; i < 8; i++) {
                D.s.notify();
                System.out.print(i + " ");
                try {
                    D.s.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class B extends Thread{
    D d = new D();
    @Override
    public void run(){
        synchronized (D.s) {
            for (char i = 'A'; i < 'H'; i++) {
                D.s.notify();
                System.out.print(i + " ");
                try {
                    D.s.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}