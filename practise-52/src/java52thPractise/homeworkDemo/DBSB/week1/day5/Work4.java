package java52thPractise.homeworkDemo.DBSB.week1.day5;

import java.util.*;

public class Work4 {
    public static void main(String[] args) {

        // // 创建年级列表，存储老师，老师对应一系列学生
        // Map<Integer,Grade> gradeMap = new HashMap<>();
        // // 创建老师列表
        // List<Teacher> teacherList = new ArrayList<>();
        // // 创建学生列表
        // List<Student> studentList = new ArrayList<>();
        //
        // // 初始化列表
        // teacherList = addTeachers(teacherList);
        // studentList = addStudents(studentList);
        //
        // gradeMap = addGrade(teacherList,studentList,gradeMap);
        //
        // Scanner scanner = new Scanner(System.in);
        // System.out.println("请输入您想查询的教师id:");
        // int teacherId = scanner.nextInt();
        // // 根据用户输入的id返回对应的教师以及学生列表
        // Grade grade = gradeMap.get(teacherId);
        //
        // // 输出
        // System.out.print("您查询的老师为：");
        // System.out.println(grade.teacher);
        // System.out.print("您查询的老师所教的学生：");
        // System.out.println(grade.studentList);



        //******************************************************************************

        // // 创建年级列表，存储老师，老师对应一系列学生
        // Map<Integer,Grade> gradeMap = new HashMap<>();
        // // 创建老师列表
        // List<Teacher> teacherList = new ArrayList<>();
        // // 创建学生列表
        // List<Student> studentList = new ArrayList<>();
        //
        // // 初始化列表
        // teacherList = addTeachers(teacherList);
        // studentList = addStudents(studentList);
        //
        // Scanner scanner = new Scanner(System.in);
        // System.out.println("请输入您想查询的教师id:");
        // int teacherId = scanner.nextInt();
        // Grade grade = new Grade();
        // grade = newGrade(studentList,teacherList,teacherId,grade);
        //
        // System.out.print("您查询的老师为：");
        // System.out.println(grade.teacher);
        // System.out.print("您查询的老师所教的学生：");
        // System.out.println(grade.studentList);

        // ******************************************************************

        // 创建教师id，教师集合
        Map<Integer,Teacher> teacherMap = new HashMap<>();
        // 创建教师id，学生集合
        Map<Integer,List<Student>> studentMap = new HashMap<>();
        List<Student> studentList = new ArrayList<>();
        studentList = addStudents(studentList);

        teacherMap = putTeacher(teacherMap);
        studentMap = putStudents(studentMap,studentList);

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您想查询的教师id:");
        int teacherId = scanner.nextInt();




    }

    private static Map<Integer, List<Student>> putStudents(Map<Integer, List<Student>> studentMap, List<Student> studentList) {
        for (Student student : studentList) {

        }
        return null;
    }


    private static Map<Integer, Teacher> putTeacher(Map<Integer, Teacher> teacherMap) {
        teacherMap.put(1,new Teacher(1,"马云",25));
        teacherMap.put(2,new Teacher(2,"马化腾",23));
        teacherMap.put(3,new Teacher(3,"刘强东",24));
        teacherMap.put(4,new Teacher(4,"俞敏洪",28));
        return teacherMap;
    }

    private static Grade newGrade(List<Student> studentList, List<Teacher> teacherList, int teacherId, Grade grade) {
        List<Student> students = new ArrayList<>();
        Teacher teacher = teacherList.get(teacherId);
        for (Student student : studentList) {
            if(student.getTeacher_id() == teacherId){
                students.add(student);
            }
        }
        grade = new Grade(teacher,students);
        return grade;
    }


    private static Map<Integer, Grade> addGrade(List<Teacher> teacherList, List<Student> studentList, Map<Integer, Grade> gradeMap) {

        // 匹配老师id和学生信息中的teacher_id，生成老师及其对应的学生列表
        for (Teacher teacher : teacherList) {
            List<Student> students = new ArrayList<>();
            for (Student student : studentList) {
                if(student.getTeacher_id() == teacher.getId()){
                    students.add(student);
                }
            }

            // 到这里 students对应了这个老师带的学生

            // System.out.println(students);
            gradeMap.put(teacher.getId(),new Grade(teacher,students));
            // 清空学生列表，重新添加下一个老师的学生列表
        }
        return gradeMap;
    }


    private static List<Student> addStudents(List<Student> studentList) {
        studentList.add(new Student(1,"光头强",11,1));
        studentList.add(new Student(2,"熊大",8,2));
        studentList.add(new Student(3,"熊二",7,2));
        studentList.add(new Student(4,"章泽天",12,3));
        studentList.add(new Student(5,"范冰冰",11,3));
        studentList.add(new Student(6,"喜洋洋",12,4));
        studentList.add(new Student(7,"美羊羊",12,4));
        studentList.add(new Student(8,"沸羊羊",11,4));
        return studentList;
    }

    private static List<Teacher> addTeachers(List<Teacher> teacherList) {
        teacherList.add(new Teacher(1,"马云",25));
        teacherList.add(new Teacher(2,"马化腾",23));
        teacherList.add(new Teacher(3,"刘强东",24));
        teacherList.add(new Teacher(4,"俞敏洪",28));
        return teacherList;
    }
}

class Grade{
    Teacher teacher;
    List<Student> studentList;

    public Grade() {
    }

    public Grade(Teacher teacher, List<Student> studentList) {
        this.teacher = teacher;
        this.studentList = studentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Grade)) return false;

        Grade grade = (Grade) o;

        if (teacher != null ? !teacher.equals(grade.teacher) : grade.teacher != null) return false;
        return studentList != null ? studentList.equals(grade.studentList) : grade.studentList == null;
    }

    @Override
    public int hashCode() {
        int result = teacher != null ? teacher.hashCode() : 0;
        result = 31 * result + (studentList != null ? studentList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Grade{");
        sb.append("teacher=").append(teacher);
        sb.append(", studentList=").append(studentList);
        sb.append('}');
        return sb.toString();
    }
}

class Student{
    private int id;
    private String name;
    private int age;
    private int teacher_id;

    public Student(int id, String name, int age, int teacher_id) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.teacher_id = teacher_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (age != student.age) return false;
        if (teacher_id != student.teacher_id) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + teacher_id;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", teacher_id=").append(teacher_id);
        sb.append('}');
        return sb.toString();
    }
}

class Teacher{
    private int id;
    private String name;
    private int age;

    public Teacher(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;

        Teacher teacher = (Teacher) o;

        if (id != teacher.id) return false;
        if (age != teacher.age) return false;
        return name != null ? name.equals(teacher.name) : teacher.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Teacher{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}