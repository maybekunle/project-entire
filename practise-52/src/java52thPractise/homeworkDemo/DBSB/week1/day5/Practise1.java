package java52thPractise.homeworkDemo.DBSB.week1.day5;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Practise1 {
    public static void main(String[] args) {
        Map<Integer,Integer> map = new HashMap<>();
        Random random = new Random();
        Integer value = 0;
        for (int i = 0; i < 10000; i++) {
            int index = random.nextInt(20)+1;
            if(!map.containsKey(index)){
                map.put(index,0);
            }
            value = map.get(index) + 1;
            map.put(index,value);
        }
        System.out.println(map);
    }
}
