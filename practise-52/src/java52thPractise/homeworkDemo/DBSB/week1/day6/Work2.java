package java52thPractise.homeworkDemo.DBSB.week1.day6;

import java.util.ArrayList;
import java.util.List;

public class Work2 {
    public static void main(String[] args) {
        String s = "abcd";
        String t = "baedc";

        List<Character> list1 = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            list1.add(s.charAt(i));
        }
        char c = 0;
        for (int i = 0; i < t.length(); i++) {
            if(!list1.contains(t.charAt(i))){
                c = t.charAt(i);
            }
        }
        System.out.println(c);
    }
}
