package java52thPractise.homeworkDemo.DBSB.week1.day6;

import java.util.HashMap;
import java.util.Map;

public class Work4 {
    public static void main(String[] args) {
        String s = "aabacabcdabcde";
        Map<Character,Integer> charMap = countOfChar(s);
        System.out.println(charMap);
    }

    private static Map<Character, Integer> countOfChar(String s) {
        Map<Character,Integer> countMap = new HashMap<>();
        // int count = 1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(countMap.containsKey(c)){
                int x = countMap.get(c);
                countMap.put(c,++x);
            }else {
                countMap.put(c,1);
            }
        }
        return countMap;
    }
}
