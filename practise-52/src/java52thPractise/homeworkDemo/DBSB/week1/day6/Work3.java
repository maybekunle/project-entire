package java52thPractise.homeworkDemo.DBSB.week1.day6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Work3 {
    public static void main(String[] args) {
        String[] words = {"aat","cat","bt","hat","tree","att"};
        String chars = "ataach";

        int x = numbersOfGoodWords(words,chars);
        System.out.println(x);

    }

    private static int numbersOfGoodWords(String[] words, String chars) {
        int count = 0;
        for (String word : words) {
            if (isGoodWords(word, chars)) {
                count = count + word.length();
            }
        }
        return count;
    }

    private static boolean isGoodWords(String word, String chars) {
        HashMap<Character,Integer> charsMap = new HashMap<>();
        for (int i = 0; i < chars.length(); i++) {
            Integer count = charsMap.getOrDefault(chars.charAt(i), 1);
            charsMap.put(chars.charAt(i),++count); // ++++++   count++ 错误，没运算
        }

        for (int i = 0; i < word.length(); i++) {
            if(charsMap.containsKey(word.charAt(i))){
                Integer integer = charsMap.get(word.charAt(i));
                charsMap.put(word.charAt(i),integer-1);
                if(charsMap.get(word.charAt(i)) <= 0){
                    return false;
                }
            }else {
                return false;
            }
        }

        return true;
    }


}
