package java52thPractise.homeworkDemo.DBSB.week1.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class Work2_1 {
    public static void main(String[] args) {

        Collection<User> con = new ArrayList<>();
        con.add(new User("赵一",18));
        con.add(new User("李二",18));
        con.add(new User("王三",18));
        con.add(new User("赵四",18));
        con.add(new User("赵一",18));
        con.add(new User("李二",18));

        Collection<User> con2 = toHeavyList(con);
        System.out.println(con2);
    }

    private static Collection<User> toHeavyList(Collection<User> con) {
        Collection<User> con3 = new ArrayList<>();
        Iterator<User> iterator = con.iterator();
        while (iterator.hasNext()){
            User next = iterator.next();
            if (con3.contains(next)){
                iterator.remove();
                continue;
            }
            con3.add(next);
        }

        return con;
    }
}

class User{
    String name;
    int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (age != user.age) return false;
        return name != null ? name.equals(user.name) : user.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}