package java52thPractise.homeworkDemo.DBSB.week1.day2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class Work3 {
    public static void main(String[] args) throws ParseException {
        Collection<Order> orders = new ArrayList<>();
        orders.add(new Order(1,9,
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-26"),
                new OrderStage(true,false,false),
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-27")));

        orders.add(new Order(2,88,
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-03-26"),
                new OrderStage(true,true,false),
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-03-27")));

        orders.add(new Order(3,7777,
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-15"),
                new OrderStage(false,true,false),
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-16")));

        orders.add(new Order(4,66,
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-26"),
                new OrderStage(true,false,false),
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-03-26")));

        orders.add(new Order(5,555,
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-22"),
                new OrderStage(false,true,false),
                new SimpleDateFormat("yyyy-MM-dd").parse("2023-02-25")));


        findEarlyOrder(orders);
        System.out.println("************************");
        findPayOrder(orders);
        System.out.println("************************");
        findTargetPriceOrder(orders);


    }

    private static void findTargetPriceOrder(Collection<Order> orders) {
        for (Order next : orders) {
            if (next.price > 200 && next.orderStage.isSendOut) {
                System.out.println(next);
            }
        }
    }

    private static void findPayOrder(Collection<Order> orders) {
        for (Order next : orders) {
            if (next.orderStage.isPayment) {
                System.out.println(next);
            }
        }
    }

    private static void findEarlyOrder(Collection<Order> orders) {

        Order order = new Order();
        Iterator<Order> iterator = orders.iterator();
        Order next = iterator.next();
        Date date = next.orderTime;
        while (iterator.hasNext()){
            next = iterator.next();
            if(next.orderTime.before(date)){
                date = next.orderTime;
                order = next;
            }

        }
        System.out.println(order);
    }


}

class Order{
    int id;
    double price;
    Date orderTime;
    OrderStage orderStage;
    Date updateTime;

    public Order() {
    }

    public Order(int id, double price, Date orderTime, OrderStage orderStage, Date updateTime) {
        this.id = id;
        this.price = price;
        this.orderTime = orderTime;
        this.orderStage = orderStage;
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Order{");
        sb.append("id=").append(id);
        sb.append(", price=").append(price);
        sb.append(", orderTime=").append(orderTime);
        sb.append(", orderStage=").append(orderStage);
        sb.append(", updateTime=").append(updateTime);
        sb.append('}');
        return sb.toString();
    }
}

class OrderStage {
    boolean isPayment;
    boolean isSendOut;
    boolean isEvaluation;

    public OrderStage(boolean isPayment, boolean isSendOut, boolean isEvaluation) {
        this.isPayment = isPayment;
        this.isSendOut = isSendOut;
        this.isEvaluation = isEvaluation;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OrderStage{");
        sb.append("isPayment=").append(isPayment);
        sb.append(", isSendOut=").append(isSendOut);
        sb.append(", isEvaluation=").append(isEvaluation);
        sb.append('}');
        return sb.toString();
    }
}