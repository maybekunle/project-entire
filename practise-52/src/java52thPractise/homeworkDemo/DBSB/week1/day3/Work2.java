package java52thPractise.homeworkDemo.DBSB.week1.day3;

import java.util.ArrayList;
import java.util.List;

public class Work2 {
    public static void main(String[] args) {
        List<User> list = new ArrayList<>();
        list.add(new User("赵一",18));
        list.add(new User("李二",18));
        list.add(new User("王三",18));
        list.add(new User("赵四",18));
        list.add(new User("赵一",18));

        List<User> list1 = toHeavyUser(list);
        System.out.println(list1);

    }

    private static List<User> toHeavyUser(List<User> list) {
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            User user = list.get(i);
            if(!userList.contains(user)){
                userList.add(user);
            }
        }

        return userList;
    }
}

class User{
    private String name;
    private int age;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (age != user.age) return false;
        return name != null ? name.equals(user.name) : user.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
