package java52thPractise.homeworkDemo.DBSB.week1.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Work3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入一串字母：");
        String s = scanner.nextLine();

        // 去重
        String s1 = toHeavyString(s);

        System.out.println(s1);
    }

    private static String toHeavyString(String s) {

        List<String> strings = new ArrayList<>();
        // 遍历字符串，放入集合
        for (int i = 0; i < s.length(); i++) {
            String c = String.valueOf(s.charAt(i));
            strings.add(c);
        }

        // 利用集合去重
        List<String> list = toHeavyList(strings);

        // 把集合中元素放入字符串
        StringBuffer l = new StringBuffer();
        for (String value : list) {
            l.append(value);
        }

        return l.toString();
    }

    private static List<String> toHeavyList(List<String> strings) {
        List<String> s = new ArrayList<>();
        for (String c : strings) {
            if (!s.contains(c)) {
                s.add(c);
            }
        }
        return s;
    }


}
