package java52thPractise.homeworkDemo.DBSB.week1.day3;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.function.Consumer;

public class Work4 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("zs");
        list.add("ls");
        list.add("wu");
        list.add("zl");

        System.out.println("11111111111111");
        // 迭代器遍历
        ListIterator<String> stringListIterator = list.listIterator();
        while (stringListIterator.hasNext()){
            String s = stringListIterator.next();
            System.out.println(s);
        }

        System.out.println("22222222222222");
        // 增强for遍历
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("33333333333333");
        // for循环遍历
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            System.out.println(s);
        }

        System.out.println("444444444444");
        // fotEach 遍历
        list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        System.out.println("55555555555555");
        // toArray遍历
        Object[] objects = list.toArray();
        for (Object object : objects) {
            System.out.println(object);
        }

    }
}
