package java52thPractise.homeworkDemo.DBSB.week1.day4;

import java.util.ArrayList;
import java.util.Scanner;

public class Work3 {
    public static void main(String[] args) {
        ArrayList<Goods> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("按提示输入：");
            System.out.println("添加商品，请按1");
            System.out.println("删除商品，请按2");
            System.out.println("展示商品，请按3");
            System.out.println("退出商城，请按4");
            int x = scanner.nextInt();
            switch (x) {
                case 1:
                    addGoods(list);
                    break;
                case 2:
                    deleteGoods(list);
                    break;
                case 3:
                    showGoods(list);
                    break;
                case 4:
                    System.out.println("即将退出程序，记得好评呦！");
                    return;
                default:
                    System.out.println("请按提示输入！");
                    break;
            }
        }

    }

    private static void showGoods(ArrayList<Goods> list) {
        for (Goods goods : list) {
            System.out.println(goods);
        }
    }

    private static void deleteGoods(ArrayList<Goods> list) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("您看哪个不顺眼告诉我：");
        String name = scanner.nextLine();
        // 遍历，查找对应商品，删除
        for (int i = 0; i < list.size(); i++) {
            Goods student = list.get(i);
            if (student.getName().equals(name)) {
                list.remove(student);
                System.out.println("我帮您把商品删了！");
                return;
            }
        }
        System.out.println("是这个吗？没这个商品啊。");
    }

    private static void addGoods(ArrayList<Goods> list) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入商品名字：");
        String name = scanner.nextLine();
        System.out.println("请输入商品价格：");
        int price = scanner.nextInt();

        // 添加商品
        list.add(new Goods(name, price));
        System.out.println("添加完毕。");
    }
}


class Goods {
    private String name;
    private int price;

    public Goods(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Goods)) return false;

        Goods goods = (Goods) o;

        if (price != goods.price) return false;
        return name != null ? name.equals(goods.name) : goods.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + price;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Goods{");
        sb.append("name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}