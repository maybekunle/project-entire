package java52thPractise.homeworkDemo.DBSB.week1.day4;

import java.util.ArrayList;
import java.util.Scanner;

public class Work2 {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("按提示输入：");
            System.out.println("添加学生，请按1");
            System.out.println("删除学生，请按2");
            System.out.println("展示学生，请按3");
            System.out.println("退出程序，请按4");
            int n = scanner.nextInt();
            switch (n) {
                case 1:
                    addStudent(list);
                    break;
                case  2:
                    deleteStudent(list);
                    break;
                case 3:
                    showStudent(list);
                    break;
                case 4:
                    System.out.println("即将退出程序，记得好评呦！");
                    return;
                default:
                    System.out.println("请按提示输入！");
                    break;
            }
        }

    }

    private static void showStudent(ArrayList<Student> list) {
        for (Student student : list) {
            System.out.println(student);
        }
    }

    private static void deleteStudent(ArrayList<Student> list) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("您看哪个不顺眼告诉我：");
        String name = scanner.nextLine();
        // 遍历，查找对应学生，删除
        for (int i = 0; i < list.size(); i++) {
            Student student = list.get(i);
            if(student.getName().equals(name)){
                list.remove(student);
                System.out.println("我帮您把他删了！");
                return;
            }
        }
        System.out.println("是他吗？没这个人啊。");
    }

    private static void addStudent(ArrayList<Student> list) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入学生姓名：");
        String name = scanner.nextLine();
        System.out.println("请输入学生年龄：");
        int age = scanner.nextInt();

        // 添加学生
        list.add(new Student(name,age));
        System.out.println("添加完毕。");
    }
}

class Student{
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (age != student.age) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}