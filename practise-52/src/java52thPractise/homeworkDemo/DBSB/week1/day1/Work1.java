package java52thPractise.homeworkDemo.DBSB.week1.day1;

public class Work1 {
    public static void main(String[] args) {
        Node a = new Node(7);
        Node b = new Node(6);
        Node c = new Node(5, a);
        Node d = new Node(4, b);
        Node e = new Node(3, c);
        Node f = new Node(2, d);
        Node g = new Node(1, e);
        // 合并链表
        mergeNode(f, g);

    }

    private static void mergeNode(Node f, Node g) {


       // Node head = f.date < g.date ? f : g;
        Node head =  new Node(0);
        // Node head = null; ?????????????????????????
        Node last = head;
        while (f != null && g != null) {
            if (f.date < g.date) {
                head.next = f;
                f = f.next;
            } else {
                head.next= g;
                g = g.next;
            }
            head = head.next;
        }

        if (f == null) {
            head.next = g;
        } else {
            head.next = f;
        }
        while (last.next != null) {
            System.out.print(last.next.date + "  ");
            last.next = last.next.next;
        }


    }
}


class Node {
    int date;
    Node next;

    public Node(int date) {
        this.date = date;
    }

    public Node(int date, Node next) {
        this.date = date;
        this.next = next;
    }


}