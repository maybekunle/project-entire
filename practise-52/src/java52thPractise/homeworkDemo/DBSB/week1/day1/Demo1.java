package java52thPractise.homeworkDemo.DBSB.week1.day1;

import java.util.Arrays;

public class Demo1 {
    public static void main(String[] args) {
        Integer[] nums = new Integer[10];
        nums[0] = 10;
        nums[1] = 8;
        nums[2] = 5;
        nums[3] = 18;
        nums[4] = 30;

        nums[5] = 99;

        System.out.println(Arrays.toString(nums));

        // addNum(nums,5,99);
        // addNum(nums,2,199);
        // addNum(nums,0,299);
        //
        // System.out.println(Arrays.toString(nums));
        //
        // deleteNum(nums,3);
        // deleteNum(nums,0);
        // System.out.println(Arrays.toString(nums));

        findNum(nums,5);
        findNum(nums,18);


    }

    private static void findNum(Integer[] nums, int i) {
        for (int j = 0; j < nums.length - 1; j++) {
            if (nums[j] == i){
                System.out.println(j);
                return;
            }
        }
    }

    private static void deleteNum(Integer[] nums, int i) {
        for (int j = i; j < nums.length-1; j++) {
            nums[j] = nums[j+1];
        }
    }

    private static void addNum(Integer[] nums, Integer index, Integer num) {
        Integer temp = 0;
        int count = 0;

        for (Integer integer : nums) {
            if (integer != null) {
                count++;
                if (count == nums.length) {
                    System.out.println("数组满了！");
                    return;
                } else {
                    break;
                }
            }
        }

        for (int i = count; i > index; i--) {
            nums[i] = nums[i - 1];
        }
        nums[index] = num;
    }
}
