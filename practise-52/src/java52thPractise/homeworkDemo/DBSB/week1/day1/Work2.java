package java52thPractise.homeworkDemo.DBSB.week1.day1;

import java.util.Scanner;

public class Work2 {
    public static void main(String[] args) {
        LNode a = new LNode(1);
        LNode b = new LNode(2,a);
        LNode c = new LNode(3,b);
        LNode d = new LNode(4,c);
        LNode e = new LNode(5,d);
        LNode f = new LNode(6,e);
        LNode g = new LNode(7,f);
        LNode h = new LNode(8,g);

        Scanner scanner = new Scanner(System.in);
        System.out.println("寻找倒数第几个？");
        int index = scanner.nextInt();
        LNode x = findNode(h,index);
        System.out.println(x.date);
    }

    private static LNode findNode(LNode h, int index) {
        int count = 1;
        int number = 0;
        LNode head = h;
        while (h.next != null){
            count++;
            h = h.next;
        }
        if(count < index){
            System.out.println("没这么多结点");
            return null;
        }else {
            while (true){
                if(number == count - index){
                   return head;
                }
                head = head.next;
                number++;
            }
        }
    }
}

class LNode {
    int date;
    LNode next;

    public LNode(int date) {
        this.date = date;
    }

    public LNode(int date, LNode next) {
        this.date = date;
        this.next = next;
    }
}