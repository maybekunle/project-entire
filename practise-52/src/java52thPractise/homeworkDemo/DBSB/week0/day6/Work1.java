package java52thPractise.homeworkDemo.DBSB.week0.day6;

public class Work1 {
    public static void main(String[] args) {
        // a --> b --> c --> d --> e
        Node e = new Node("e");
        Node d = new Node("d", e);
        Node c = new Node("c", d);
        Node b = new Node("b", c);
        Node a = new Node("a", b);

        // a --> b --> c --> d --> e --> c --> d --> e --> ...
       // e.next = c;

        boolean bool = hasCircle(a);
        System.out.println(bool);
    }

    private static boolean hasCircle(Node node) {

        // 快慢指针；两个指针，一个走的快一个走得慢，若是能重合，说明有环
        Node l = node;
        Node f = node;
        while (f.next != null && f.next.next != null){
            l = l.next;
            f = f.next.next;
            if(l == f){
                return true;
            }
        }
        return false;
    }
}

class Node{
    String date;
    Node next;

    public Node(String date, Node next) {
        this.date = date;
        this.next = next;
    }

    public Node(String date) {
        this.date = date;
    }
}