package java52thPractise.homeworkDemo.DBSB.week2.day6;

import java.util.HashMap;
import java.util.Map;

public class Work4 {
    public static void main(String[] args) {
        String s = "aababcabcdabcde";
        Map<Character,Integer> charTimesMap = timesOfChar(s);
        System.out.println(charTimesMap);
    }

    private static Map<Character, Integer> timesOfChar(String s) {
        Map<Character,Integer> timesMap = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            timesMap.put(s.charAt(i),timesMap.getOrDefault(s.charAt(i),0) + 1);
        }
        return timesMap;
    }
}
