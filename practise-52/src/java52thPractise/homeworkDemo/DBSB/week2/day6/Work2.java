package java52thPractise.homeworkDemo.DBSB.week2.day6;

import java.util.HashMap;
import java.util.Map;

public class Work2 {
    public static void main(String[] args) {
        String s = "abcd";
        String t = "badcb";
        char c = fingRedundatChar(s,t);
        System.out.println(c);
    }

    private static char fingRedundatChar(String s, String t) {
        Map<Character,Integer> sMap = new HashMap<>();
        Map<Character,Integer> tMap = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            Integer orDefault = sMap.getOrDefault(s.charAt(i), 0);
            sMap.put(s.charAt(i),++orDefault);
        }

        for (int i = 0; i < t.length(); i++) {
            if(sMap.containsKey(t.charAt(i))){
                Integer integer = sMap.get(t.charAt(i));
                sMap.put(t.charAt(i),--integer);
                if(sMap.get(t.charAt(i)) < 0){
                    return t.charAt(i);
                }
            }else {
                return t.charAt(i);
            }
        }
        return 0;
    }
}
