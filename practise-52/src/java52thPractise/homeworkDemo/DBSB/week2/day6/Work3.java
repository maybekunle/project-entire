package java52thPractise.homeworkDemo.DBSB.week2.day6;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Work3 {
    public static void main(String[] args) {
        String[] words = {"cat","bt","hat","tree","att","atac"};
        String chars = "atach";
        int number = theNumberOfGoodWords(words,chars);
        System.out.println(number);
    }

    private static int theNumberOfGoodWords(String[] words, String chars) {

        Map<String,Integer> goodWordsMap = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            if(isgoodWords(words[i],chars)){
                goodWordsMap.put(words[i],words[i].length());
            }
        }

        int count = 0;
        Set<String> set = goodWordsMap.keySet();
        for (String s : set) {
            Integer integer = goodWordsMap.get(s);
            count = count + integer;
        }

       return count;
    }

    private static boolean isgoodWords(String word, String chars) {
        Map<Character,Integer> charsMap = new HashMap<>();
        for (int i = 0; i < chars.length(); i++) {
            charsMap.put(chars.charAt(i),charsMap.getOrDefault(chars.charAt(i),0) + 1);
        }

        for (int i = 0; i < word.length(); i++) {
            if (!charsMap.containsKey(word.charAt(i))) {
                return false;
            }else {
                Integer integer = charsMap.get(word.charAt(i));
                charsMap.put(word.charAt(i),--integer);
                if(charsMap.get(word.charAt(i)) < 0){
                    return false;
                }
            }
        }
        return true;
    }
}
