package java52thPractise.homeworkDemo.DBSB.week2.day1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Work3 {
    public static void main(String[] args) {
        List<Transaction> list = TransactionRecord.transactions;

        List<Transaction> list1 = list.stream()
                .filter(transaction -> transaction.getYear() == 2011)
                .sorted((o1, o2) -> o1.getValue() - o2.getValue())
                .collect(Collectors.toList());
        System.out.println(list1);

        List<String> list2 = list.stream()
                .map(transaction -> transaction.getTrader().getCity())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(list2);

        List<Transaction> list3 = list.stream()
                .filter(transaction -> "beijing".equals(transaction.getTrader().getCity()))
                .sorted((o1, o2) -> o1.getTrader().getName().compareTo(o2.getTrader().getName()))
                .collect(Collectors.toList());
        System.out.println(list3);

        List<String> list4 = list.stream()
                .map(transaction -> transaction.getTrader().getName())
                .sorted(((o1, o2) -> o1.compareTo(o2)))
                .collect(Collectors.toList());
        System.out.println(list4);

        boolean b = list.stream()
                .anyMatch(transaction -> "beijing".equals(transaction.getTrader().getCity()));
        System.out.println(b);

        List<Integer> list5 = list.stream()
                .filter(transaction -> "beijing".equals(transaction.getTrader().getCity()))
                .map(transaction -> transaction.getValue())
                .collect(Collectors.toList());
        System.out.println(list5);

        List<Transaction> list6 = list.stream()
                .sorted((o1, o2) -> o1.getValue() - o2.getValue())
                .limit(1)
                .collect(Collectors.toList());
        System.out.println(list6);

        List<Integer> list7 = list.stream()
                .map(transaction -> transaction.getValue())
                .sorted((o1, o2) -> o2 - o1)
                .limit(1)
                .collect(Collectors.toList());
        System.out.println(list7);
    }
}
