package java52thPractise.homeworkDemo.DBSB.week2.day1;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Work2 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        List<Integer> list1 = list.stream().map(integer -> integer * integer).collect(Collectors.toList());
        System.out.println(list1);
    }
}
