package com.cskaoyan.market;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cskaoyan.market.db.mapper")
public class ProjectSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSpringbootApplication.class, args);
    }

}
