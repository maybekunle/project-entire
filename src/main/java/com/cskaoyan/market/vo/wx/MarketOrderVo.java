package com.cskaoyan.market.vo.wx;

import com.cskaoyan.market.db.domain.MarketOrderGoods;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class MarketOrderVo {
    private BigDecimal actualPrice;
    private short aftersaleStatus;
    private List<MarketOrderGoods> goodsList;
    private HandleOption handleOption;
    private Integer id;
    private boolean isGroupin;
    private String orderSn;
    private String orderStatusText;

}
