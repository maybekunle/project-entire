package com.cskaoyan.market.vo.wx;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
public class MarketCommentVo {
    private LocalDateTime addTime;
    private String adminContent;
    private String content;
    private String[] picList;
    private Map<String, Object> userInfo;
}
