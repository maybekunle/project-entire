package com.cskaoyan.market.vo.wx;

import lombok.Data;

@Data
public class HandleOption {
    private boolean aftersale;
    private boolean cancel;
    private boolean comment;
    private boolean confirm;
    private boolean delete;
    private boolean pay;
    private boolean rebuy;
    private boolean refund;
}
