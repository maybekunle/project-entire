package com.cskaoyan.market.vo.wx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponMyListVo {
    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private Double min;
    private Double discount;
    private String startTime;
    private String endTime;
    private Boolean available;
}
