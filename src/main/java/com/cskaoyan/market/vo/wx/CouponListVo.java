package com.cskaoyan.market.vo.wx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponListVo {
    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private Double discount;
    private Double min;
    private Short days;
}
