package com.cskaoyan.market.vo.wx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FootprintVo {
    private String brief;
    private String picUrl;
    private String addTime;
    private String goodsId;
    private String name;
    private Integer id;
    private Double retailPrice;
}
