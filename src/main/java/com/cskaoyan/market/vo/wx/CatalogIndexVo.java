package com.cskaoyan.market.vo.wx;

import com.cskaoyan.market.db.domain.MarketCategory;
import lombok.Data;

import java.util.List;
@Data
public class CatalogIndexVo {
    private List<MarketCategory> categoryList;
    private MarketCategory currentCategory;
    private List<MarketCategory> currentSubCategory;
}
