package com.cskaoyan.market.vo.wx;

import com.cskaoyan.market.db.domain.MarketOrderGoods;
import lombok.Data;

@Data
public class OrderDetailVo {
    private String[] expressInfo;
    private MarketOrderGoods[] orderGoods;
    private OrderInfo orderInfo;
}
