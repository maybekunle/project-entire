package com.cskaoyan.market.vo.wx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo {
    private BigDecimal actualPrice;
    private String address;
    private LocalDateTime addTime;
    private Short aftersaleStatus;
    private String consignee;
    private BigDecimal couponPrice;
    private String expCode;
    private String expName;
    private String expNo;
    private BigDecimal freightPrice;
    private BigDecimal goodsPrice;
    private HandleOption handleOption;
    private Integer id;
    private String message;
    private String mobile;
    private String orderSn;
    private String orderStatusText;




}
