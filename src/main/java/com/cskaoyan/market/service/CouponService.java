package com.cskaoyan.market.service;


import com.cskaoyan.market.vo.wx.CouponListVo;
import com.cskaoyan.market.vo.wx.CouponMyListVo;


import java.util.List;

public interface CouponService {
    List<CouponMyListVo> getMyList(Integer status, Integer page, Integer limit);

    boolean exchangeCoupon(String code);

    List<CouponListVo> queryList(Integer page, Integer limit);

    Boolean receiveCoupon(Integer couponId);

    List<CouponMyListVo> getSelectList();
}
