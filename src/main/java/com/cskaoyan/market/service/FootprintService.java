package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketFootprint;
import com.cskaoyan.market.vo.wx.FootprintVo;

import java.util.List;

public interface FootprintService {
    List<FootprintVo> queryFootprints(Integer page, Integer limit);

    void deleteFootprint(Integer id);

    List<MarketFootprint> getFootprints(Integer page, Integer limit, String userId, String goodsId, String sort, String order);
}
