package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketAddress;

import java.util.List;

public interface AdminAddressService {
    List<MarketAddress> getAddresses(Integer page, Integer limit, String name, String userId, String sort, String order);
}
