package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketStorage;

public interface StorageService {
    MarketStorage addOne(MarketStorage storage);
}
