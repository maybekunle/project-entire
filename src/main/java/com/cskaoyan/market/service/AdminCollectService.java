package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketCollect;

import java.util.List;

public interface AdminCollectService {
    List<MarketCollect> getCollects(Integer page, Integer limit, String userId, String valueId, String sort, String order);
}
