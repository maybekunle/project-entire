package com.cskaoyan.market.service.wx.impl;


import com.cskaoyan.market.db.domain.MarketComment;
import com.cskaoyan.market.db.domain.MarketCommentExample;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.db.mapper.MarketCommentMapper;
import com.cskaoyan.market.db.mapper.MarketUserMapper;
import com.cskaoyan.market.service.wx.MarketCommentService;
import com.cskaoyan.market.vo.wx.MarketCommentVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MarketCommentServiceImpl implements MarketCommentService {
    @Autowired
    private MarketCommentMapper commentMapper;
    @Autowired
    private MarketUserMapper userMapper;

    @Override
    public List<MarketCommentVo> list(Integer valueId, Integer type, Integer showType, Integer currentPage, Integer limitSize) {
        MarketCommentExample commentExample = new MarketCommentExample();
        // 表示查询所有评论
        if(showType == 0){
            commentExample.createCriteria().andDeletedEqualTo(false).andValueIdEqualTo(valueId).andTypeEqualTo(type.byteValue());

        }
        // 表示只查询有图的评论
        if(showType == 1){
            commentExample.createCriteria().andDeletedEqualTo(false).andValueIdEqualTo(valueId).
                    andTypeEqualTo(type.byteValue()).andHasPictureEqualTo(true);
        }
        PageHelper.startPage(currentPage, limitSize);
        List<MarketComment> marketComments = commentMapper.selectByExample(commentExample);


        List<MarketCommentVo> commentVos = new ArrayList<>();
        for (MarketComment marketComment : marketComments) {
            MarketCommentVo commentVo = new MarketCommentVo();
            commentVo.setAddTime(marketComment.getAddTime());
            commentVo.setAdminContent(marketComment.getAdminContent());
            commentVo.setContent(marketComment.getContent());
            commentVo.setPicList(marketComment.getPicUrls());

            Integer userId = marketComment.getUserId();
            MarketUser user = userMapper.selectByPrimaryKey(userId);
            Map<String, Object> userInfo = new HashMap<>();
            userInfo.put("avatarUrl", user.getAvatar());
            userInfo.put("nickName", user.getNickname());
            commentVo.setUserInfo(userInfo);

            commentVos.add(commentVo);
        }
        return commentVos;
    }

    @Override
    public List<MarketComment> listForPage(Integer valueId, Integer type, Integer showType, Integer currentPage, Integer limitSize) {
        MarketCommentExample commentExample = new MarketCommentExample();
        if(showType == 0){
            commentExample.createCriteria().andDeletedEqualTo(false).andValueIdEqualTo(valueId).andTypeEqualTo(type.byteValue());

        }
        if (showType == 1){
            commentExample.createCriteria().andDeletedEqualTo(false).andValueIdEqualTo(valueId).
                    andTypeEqualTo(type.byteValue()).andHasPictureEqualTo(true);
        }
        PageHelper.startPage(currentPage, limitSize);
        List<MarketComment> marketComments = commentMapper.selectByExample(commentExample);
        return marketComments;

    }

    @Override
    public Map<String, Integer> count(Integer valueId, Integer type) {
        MarketCommentExample commentExample = new MarketCommentExample();
        commentExample.createCriteria().andDeletedEqualTo(false).andValueIdEqualTo(valueId).andTypeEqualTo(type.byteValue());
        List<MarketComment> comments = commentMapper.selectByExample(commentExample);
        int picCount = 0;
        for (MarketComment comment : comments) {
            if(comment.getHasPicture()){
                picCount++;
            }
        }
        Map<String, Integer> countMap = new HashMap<>();
        countMap.put("allCount", comments.size());
        countMap.put("hasPicCount", picCount);
        return countMap;
    }

    @Override
    public MarketComment post(MarketComment comment, MarketUser user) {
        comment.setAddTime(LocalDateTime.now());
        comment.setUpdateTime(LocalDateTime.now());
        comment.setUserId(user.getId());
        comment.setDeleted(false);

        int i = commentMapper.insertSelective(comment);
        return i > 0 ? comment : null;
    }
}
