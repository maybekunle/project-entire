package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketComment;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.vo.wx.MarketCommentVo;

import java.util.List;
import java.util.Map;

public interface MarketCommentService {

    List<MarketCommentVo> list(Integer valueId, Integer type, Integer showType, Integer currentPage, Integer limitSize);

    List<MarketComment> listForPage(Integer valueId, Integer type, Integer showType, Integer currentPage, Integer limitSize);

    Map<String, Integer> count(Integer valueId, Integer type);

    MarketComment post(MarketComment comment, MarketUser user);
}
