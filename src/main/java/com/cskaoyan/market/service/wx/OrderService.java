package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketOrderGoods;
import com.cskaoyan.market.vo.wx.MarketOrderVo;
import com.cskaoyan.market.vo.wx.OrderDetailVo;
import com.cskaoyan.market.vo.wx.OrderSubmitVo;

import java.util.List;

public interface OrderService {
    List<MarketOrderVo> list(Integer showType, Integer page, Integer limit);


    int comment(String username, String content, Boolean hasPicture, Integer orderGoodsId, String[] picUrls, Integer star);

    OrderDetailVo detail(Integer orderId);

    int refund(Integer orderId);

    int cancel(Integer orderId);

    int delete(Integer orderId);

    int confirm(Integer orderId);

    MarketOrderGoods goods(Integer orderId);


}
