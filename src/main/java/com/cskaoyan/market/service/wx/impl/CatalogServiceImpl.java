package com.cskaoyan.market.service.wx.impl;

import com.cskaoyan.market.db.domain.MarketCategory;
import com.cskaoyan.market.db.domain.MarketCategoryExample;
import com.cskaoyan.market.db.mapper.MarketCategoryMapper;
import com.cskaoyan.market.service.wx.CatalogService;
import com.cskaoyan.market.vo.wx.CatalogCurrentVo;
import com.cskaoyan.market.vo.wx.CatalogIndexVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    MarketCategoryMapper categoryMapper;
    @Override
    public CatalogIndexVo index() {
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        CatalogIndexVo catalogIndexVo = new CatalogIndexVo();
        marketCategoryExample.or().andPidEqualTo(0).andDeletedEqualTo(false);
        List<MarketCategory> categoryList = categoryMapper.selectByExample(marketCategoryExample);
        catalogIndexVo.setCategoryList(categoryList);

        marketCategoryExample.clear();
        marketCategoryExample.or().andPidEqualTo(0).andDeletedEqualTo(false).andNameEqualTo(categoryList.get(0).getName());
        MarketCategory marketCategory = categoryMapper.selectOneByExample(marketCategoryExample);
        catalogIndexVo.setCurrentCategory(marketCategory);

        Integer id = marketCategory.getId();
        marketCategoryExample.clear();
        marketCategoryExample.or().andPidEqualTo(id).andDeletedEqualTo(false);
        List<MarketCategory> subCategory = categoryMapper.selectByExample(marketCategoryExample);
        catalogIndexVo.setCurrentSubCategory(subCategory);
        return catalogIndexVo;
    }

    @Override
    public CatalogCurrentVo current(Integer id) {
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        CatalogCurrentVo catalogCurrentVo = new CatalogCurrentVo();
        marketCategoryExample.or().andIdEqualTo(id).andDeletedEqualTo(false);
        MarketCategory marketCategory = categoryMapper.selectOneByExample(marketCategoryExample);
        catalogCurrentVo.setCurrentCategory(marketCategory);

        marketCategoryExample.clear();
        marketCategoryExample.or().andPidEqualTo(id).andDeletedEqualTo(false);
        List<MarketCategory> subCategory = categoryMapper.selectByExample(marketCategoryExample);
        catalogCurrentVo.setCurrentSubCategory(subCategory);
        return catalogCurrentVo;

    }
}
