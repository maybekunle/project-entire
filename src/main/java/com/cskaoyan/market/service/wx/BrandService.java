package com.cskaoyan.market.service.wx;


import com.cskaoyan.market.db.domain.MarketBrand;

import java.util.List;

public interface BrandService {
    List<MarketBrand> list(Integer page, Integer limit);

    MarketBrand detail(Integer id);
}
