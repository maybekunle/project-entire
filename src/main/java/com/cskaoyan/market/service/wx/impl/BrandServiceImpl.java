package com.cskaoyan.market.service.wx.impl;

import com.cskaoyan.market.db.domain.MarketBrand;
import com.cskaoyan.market.db.domain.MarketBrandExample;
import com.cskaoyan.market.db.mapper.MarketBrandMapper;
import com.cskaoyan.market.service.wx.BrandService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    MarketBrandMapper brandMapper;

    @Override
    public List<MarketBrand> list(Integer page, Integer limit) {
        MarketBrandExample marketBrandExample = new MarketBrandExample();
        marketBrandExample.or().andDeletedEqualTo(false);
        PageHelper.startPage(page, limit);
        List<MarketBrand> marketBrands = brandMapper.selectByExample(marketBrandExample);
        return marketBrands;
    }

    @Override
    public MarketBrand detail(Integer id) {
        MarketBrandExample marketBrandExample = new MarketBrandExample();
        marketBrandExample.or().andDeletedEqualTo(false).andIdEqualTo(id);
        MarketBrand brand = brandMapper.selectOneByExample(marketBrandExample);
        return brand;
    }

}
