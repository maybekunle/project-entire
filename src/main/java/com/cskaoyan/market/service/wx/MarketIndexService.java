package com.cskaoyan.market.service.wx;

import java.util.Map;

public interface MarketIndexService {
    Map<String, Object> getAllIndexInfo();
}
