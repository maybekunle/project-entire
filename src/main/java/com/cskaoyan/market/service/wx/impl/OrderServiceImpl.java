package com.cskaoyan.market.service.wx.impl;

import com.cskaoyan.market.db.domain.*;
import com.cskaoyan.market.db.mapper.*;
import com.cskaoyan.market.service.wx.OrderService;
import com.cskaoyan.market.vo.wx.*;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    MarketOrderMapper orderMapper;
    @Autowired
    MarketOrderGoodsMapper orderGoodsMapper;
    @Autowired
    MarketCommentMapper commentMapper;
    @Autowired
    MarketUserMapper userMapper;
    @Autowired
    MarketAddressMapper addressMapper;
    @Autowired
    MarketCartMapper cartMapper;
    @Autowired
    MarketSystemMapper systemMapper;
    @Autowired
    MarketCouponMapper couponMapper;

    @Override
    public List<MarketOrderVo> list(Integer showType, Integer page, Integer limit) {
        MarketOrderExample marketOrderExample = new MarketOrderExample();
        MarketOrderExample.Criteria criteria = marketOrderExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketOrder> marketOrderList = new LinkedList<>();
        PageHelper.startPage(page, limit);
        if (showType == 0) {
            marketOrderList = orderMapper.selectByExample(marketOrderExample);
        }
        if (showType == 1) {
            criteria.andOrderStatusEqualTo((short) 101);
            marketOrderList = orderMapper.selectByExample(marketOrderExample);
        }
        if (showType == 2) {
            criteria.andOrderStatusEqualTo((short) 201);
            marketOrderList = orderMapper.selectByExample(marketOrderExample);
        }
        if (showType == 3) {
            criteria.andOrderStatusEqualTo((short) 301);
            marketOrderList = orderMapper.selectByExample(marketOrderExample);
        }
        if (showType == 4) {
            criteria.andOrderStatusBetween((short) 401, (short) 402);
            marketOrderList = orderMapper.selectByExample(marketOrderExample);
        }
        List<MarketOrderVo> result = new ArrayList<>();

        for (int i = 0; i < marketOrderList.size(); i++) {
            MarketOrderVo marketOrderVo = new MarketOrderVo();
            //设置订单状态
            String status = status(marketOrderList.get(i));
            marketOrderVo.setOrderStatusText(status);

            Short afterSaleStatus = marketOrderList.get(i).getAftersaleStatus();
            marketOrderVo.setAftersaleStatus(afterSaleStatus);

            String orderSn = marketOrderList.get(i).getOrderSn();
            marketOrderVo.setOrderSn(orderSn);

            BigDecimal actualPrice = marketOrderList.get(i).getActualPrice();
            marketOrderVo.setActualPrice(actualPrice);

            Integer id1 = marketOrderList.get(i).getId();
            marketOrderVo.setId(id1);

            Integer id = marketOrderVo.getId();

            MarketOrderGoodsExample marketOrderGoodsExample = new MarketOrderGoodsExample();
            marketOrderGoodsExample.or().andOrderIdEqualTo(id);
            List<MarketOrderGoods> orderGoods = orderGoodsMapper.selectByExample(marketOrderGoodsExample);
            marketOrderVo.setGoodsList(orderGoods);

            HandleOption handleOption = getHandleOption(marketOrderList.get(i));
            marketOrderVo.setHandleOption(handleOption);
            result.add(marketOrderVo);
        }
        return result;
    }

    @Override
    public int comment(String username, String content, Boolean hasPicture, Integer orderGoodsId, String[] picUrls, Integer star) {
        MarketUserExample userExample = new MarketUserExample();
        userExample.or().andUsernameEqualTo(username);
        MarketUser marketUser = userMapper.selectOneByExample(userExample);
        Integer userId = marketUser.getId();

        MarketOrderGoodsExample orderGoodsExample = new MarketOrderGoodsExample();
        orderGoodsExample.or().andOrderIdEqualTo(orderGoodsId);
        MarketOrderGoods orderGoods = orderGoodsMapper.selectOneByExample(orderGoodsExample);


        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderGoodsId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        if(marketOrder.getComments()<1){
            return 726;
        }
        marketOrder.setComments((short) (marketOrder.getComments() - 1));
        marketOrder.setUpdateTime(LocalDateTime.now());
        orderMapper.updateByExample(marketOrder, orderExample);


        MarketComment marketComment = new MarketComment();
        marketComment.setValueId(0);
        marketComment.setType((byte) 0);
        marketComment.setContent(content);
        marketComment.setUserId(userId);
        marketComment.setHasPicture(hasPicture);
        marketComment.setPicUrls(picUrls);
        marketComment.setStar(star.shortValue());
        marketComment.setAddTime(LocalDateTime.now());
        marketComment.setUpdateTime(LocalDateTime.now());
        marketComment.setDeleted(false);
        try {
            commentMapper.insert(marketComment);
            orderGoods.setComment(marketComment.getId());
            orderGoods.setUpdateTime(LocalDateTime.now());
            orderGoodsMapper.updateByExample(orderGoods, orderGoodsExample);
        } catch (Exception e) {
            return 726;
        }
        return 200;
    }

    @Override
    public OrderDetailVo detail(Integer orderId) {
        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        String expName = "";
        String shipChannel = "";
        if (marketOrder.getShipChannel() != null) {
            shipChannel = marketOrder.getShipChannel();
        }

        switch (shipChannel) {
            case "YTO":
                expName = "圆通速递";
                break;
            case "YD":
                expName = "韵达速递";
                break;
            case "YZPY":
                expName = "邮政快递";
                break;
            case "EMS":
                expName = "EMS";
                break;
            case "DBL":
                expName = "德邦快递";
                break;
            case "FAST":
                expName = "快捷快递";
                break;
            case "ZJS":
                expName = "宅急送";
                break;
            case "TNT":
                expName = "TNT快递";
                break;
            case "UPS":
                expName = "UPS";
                break;
            case "DHL":
                expName = "DHL";
                break;
            case "FEDEX":
                expName = "FEDEX联邦(国内件)";
                break;
            case "FEDEX_GJ":
                expName = "FEDEX联邦(国际件)";
                break;
        }


        OrderInfo orderInfo = new OrderInfo(marketOrder.getActualPrice(), marketOrder.getAddress(),
                marketOrder.getAddTime(), marketOrder.getAftersaleStatus(), marketOrder.getConsignee(),
                marketOrder.getCouponPrice(), shipChannel, expName, marketOrder.getShipSn(), marketOrder.getFreightPrice(), marketOrder.getGoodsPrice(),
                getHandleOption(marketOrder), marketOrder.getId(), marketOrder.getMessage(), marketOrder.getMobile(),
                marketOrder.getOrderSn(), status(marketOrder));

        orderInfo.setOrderStatusText(status(marketOrder));
        orderInfo.setHandleOption(getHandleOption(marketOrder));

        OrderDetailVo orderDetailVo = new OrderDetailVo();
        orderDetailVo.setOrderInfo(orderInfo);

        MarketOrderGoodsExample orderGoodsExample = new MarketOrderGoodsExample();
        orderGoodsExample.or().andOrderIdEqualTo(orderId);
        List<MarketOrderGoods> goodsList = orderGoodsMapper.selectByExample(orderGoodsExample);
        MarketOrderGoods[] orderGoods = new MarketOrderGoods[goodsList.size()];
        int i = 0;
        for (MarketOrderGoods marketOrderGoods : goodsList) {
            orderGoods[i++] = marketOrderGoods;
        }
        orderDetailVo.setOrderGoods(orderGoods);
        orderDetailVo.setExpressInfo(new String[]{});
        return orderDetailVo;
    }

    @Override
    public int refund(Integer orderId) {
        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        marketOrder.setOrderStatus((short) 202);
        marketOrder.setUpdateTime(LocalDateTime.now());
        try {
            orderMapper.updateByExample(marketOrder, orderExample);
        } catch (Exception e) {
            return 500;
        }
        return 200;
    }

    @Override
    public int cancel(Integer orderId) {
        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        marketOrder.setOrderStatus((short) 102);
        marketOrder.setUpdateTime(LocalDateTime.now());
        try {
            orderMapper.updateByExample(marketOrder, orderExample);
        } catch (Exception e) {
            return 500;
        }
        return 200;
    }

    @Override
    public int delete(Integer orderId) {
        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        marketOrder.setDeleted(true);
        marketOrder.setUpdateTime(LocalDateTime.now());
        try {
            orderMapper.updateByExample(marketOrder, orderExample);
        } catch (Exception e) {
            return 500;
        }
        return 200;
    }

    @Override
    public int confirm(Integer orderId) {
        MarketOrderExample orderExample = new MarketOrderExample();
        orderExample.or().andIdEqualTo(orderId);
        MarketOrder marketOrder = orderMapper.selectOneByExample(orderExample);
        marketOrder.setOrderStatus((short) 401);
        marketOrder.setUpdateTime(LocalDateTime.now());
        try {
            orderMapper.updateByExample(marketOrder, orderExample);
        } catch (Exception e) {
            return 500;
        }
        return 200;
    }

    @Override
    public MarketOrderGoods goods(Integer orderId) {
        MarketOrderGoodsExample orderGoodsExample = new MarketOrderGoodsExample();
        orderGoodsExample.or().andIdEqualTo(orderId);
        return orderGoodsMapper.selectOneByExample(orderGoodsExample);
    }

    private static String status(MarketOrder marketOrder) {
        String orderStatus;
        if (marketOrder.getOrderStatus() == 101) {
            orderStatus = "未付款";
        } else if (marketOrder.getOrderStatus() == 102 || marketOrder.getOrderStatus() == 103) {
            orderStatus = "已取消";
        } else if (marketOrder.getOrderStatus() == 201) {
            orderStatus = "已付款";
        } else if (marketOrder.getOrderStatus() == 202) {
            orderStatus = "订单取消，退款中";
        } else if (marketOrder.getOrderStatus() == 203) {
            orderStatus = "已退款";
        } else if (marketOrder.getOrderStatus() == 301) {
            orderStatus = "已发货";
        } else {
            orderStatus = "已收货";
        }
        return orderStatus;
    }

    private static HandleOption getHandleOption(MarketOrder marketOrder) {
        HandleOption handleOption = new HandleOption();
        if (marketOrder.getOrderStatus() == 101) {
            handleOption.setPay(true);
            handleOption.setCancel(true);
        }
        if (marketOrder.getOrderStatus() == 201) {
            handleOption.setRefund(true);
        }
        if (marketOrder.getOrderStatus() == 301) {
            handleOption.setConfirm(true);
        }
        if (marketOrder.getOrderStatus() == 401 || marketOrder.getOrderStatus() == 402) {
            if (marketOrder.getComments() > 0) {
                handleOption.setComment(true);
            }
            handleOption.setAftersale(true);
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        }

        return handleOption;
    }



}
