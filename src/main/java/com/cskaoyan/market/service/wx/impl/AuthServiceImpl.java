package com.cskaoyan.market.service.wx.impl;

import com.cskaoyan.market.db.domain.MarketAdmin;
import com.cskaoyan.market.db.domain.MarketAdminExample;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.db.domain.MarketUserExample;
import com.cskaoyan.market.db.mapper.MarketAdminMapper;
import com.cskaoyan.market.db.mapper.MarketUserMapper;
import com.cskaoyan.market.service.wx.AuthService;
import com.cskaoyan.market.vo.wx.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    MarketUserMapper marketUserMapper;

    @Resource
    MarketAdminMapper marketAdminMapper;

    @Override
    public UserInfoVo selectUserInfoByUsername(String username) {
        MarketUserExample marketUserExample = new MarketUserExample();
        marketUserExample.or().andUsernameEqualTo(username);
        MarketUser marketUsers = marketUserMapper.selectOneByExample(marketUserExample);
        return new UserInfoVo(marketUsers.getUsername(), marketUsers.getAvatar());
    }

    @Override
    public String selectPasswordByUsername(String username) {
        MarketUserExample marketUserExample = new MarketUserExample();
        marketUserExample.or().andUsernameEqualTo(username);
        MarketUser marketUser = marketUserMapper.selectOneByExample(marketUserExample);
        return marketUser.getPassword();

    }

    @Override
    public MarketUser selectUser(String username) {
        MarketUserExample userExample = new MarketUserExample();
        userExample.or().andUsernameEqualTo(username);
        List<MarketUser> users = marketUserMapper.selectByExample(userExample);
        MarketUser user = users.get(0);
        return user;
    }

    @Override
    public UserInfoVo selectAdminInfoByUsername(String username) {
        MarketAdminExample adminExample = new MarketAdminExample();
        adminExample.or().andUsernameEqualTo(username);
        List<MarketAdmin> marketAdmins = marketAdminMapper.selectByExample(adminExample);
        MarketAdmin marketAdmin = marketAdmins.get(0);
        UserInfoVo userInfoVo = new UserInfoVo();
        userInfoVo.setAvatarUrl(marketAdmin.getAvatar());
        userInfoVo.setNickName(marketAdmin.getUsername());
        return userInfoVo;
    }
}
