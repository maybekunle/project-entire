package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.vo.wx.UserInfoVo;

public interface AuthService {
    UserInfoVo selectUserInfoByUsername(String username);

    String selectPasswordByUsername(String username);

    MarketUser selectUser(String username);

    UserInfoVo selectAdminInfoByUsername(String username);
}
