package com.cskaoyan.market.service.wx.impl;


import com.cskaoyan.market.db.domain.MarketFeedback;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.db.mapper.MarketFeedbackMapper;
import com.cskaoyan.market.service.wx.MarketFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MarketFeedbackServiceImpl implements MarketFeedbackService {
    @Autowired
    private MarketFeedbackMapper feedbackMapper;

    @Override
    public boolean submit(MarketFeedback feedback, MarketUser user) {
        feedback.setUserId(user.getId());
        feedback.setUsername(user.getUsername());
        feedback.setAddTime(LocalDateTime.now());
        feedback.setUpdateTime(LocalDateTime.now());
        feedback.setDeleted(false);

        int i = feedbackMapper.insertSelective(feedback);

        return i > 0;


    }
}
