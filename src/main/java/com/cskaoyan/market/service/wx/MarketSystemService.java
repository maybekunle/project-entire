package com.cskaoyan.market.service.wx;

import java.util.Map;

public interface MarketSystemService {
    Map<String, String> getAllSystemInfo();
}
