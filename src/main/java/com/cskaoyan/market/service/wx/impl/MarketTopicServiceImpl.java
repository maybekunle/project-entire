package com.cskaoyan.market.service.wx.impl;


import com.cskaoyan.market.db.domain.MarketGoods;
import com.cskaoyan.market.db.domain.MarketTopic;
import com.cskaoyan.market.db.domain.MarketTopicExample;
import com.cskaoyan.market.db.mapper.MarketGoodsMapper;
import com.cskaoyan.market.db.mapper.MarketTopicMapper;
import com.cskaoyan.market.service.wx.MarketTopicService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MarketTopicServiceImpl implements MarketTopicService {
    @Autowired
    private MarketTopicMapper topicMapper;
    @Autowired
    private MarketGoodsMapper goodsMapper;

    @Override
    public List<MarketTopic> list(Integer page, Integer limit) {
        MarketTopicExample topicExample = new MarketTopicExample();
        topicExample.or().andDeletedEqualTo(false);
        PageHelper.startPage(page, limit);
        List<MarketTopic> topics = topicMapper.selectByExample(topicExample);

        return topics;

    }

    @Override
    public Map<String, Object> getDetailById(Integer id) {
        MarketTopic topic = topicMapper.selectByPrimaryKey(id);
        Integer[] goodsIds = topic.getGoods();
        List<MarketGoods> goodsList = new ArrayList<>();
        for (Integer goodsId : goodsIds) {
            MarketGoods marketGoods = goodsMapper.selectByPrimaryKey(goodsId);
            goodsList.add(marketGoods);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("goods", goodsList);
        map.put("topic", topic);
        return map;
    }

    @Override
    public List<MarketTopic> getRelatedById(Integer id) {
        MarketTopicExample topicExample = new MarketTopicExample();
        topicExample.or().andDeletedEqualTo(false).andIdNotEqualTo(id);
        PageHelper.startPage(1, 4);
        List<MarketTopic> topics = topicMapper.selectByExample(topicExample);
        return topics;
    }
}
