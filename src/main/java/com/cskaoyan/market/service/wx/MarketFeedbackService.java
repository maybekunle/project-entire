package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketFeedback;
import com.cskaoyan.market.db.domain.MarketUser;

public interface MarketFeedbackService {

    boolean submit(MarketFeedback feedback, MarketUser user);
}
