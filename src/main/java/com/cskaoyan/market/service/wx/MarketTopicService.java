package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketTopic;

import java.util.List;
import java.util.Map;

public interface MarketTopicService {

    List<MarketTopic> list(Integer page, Integer limit);

    Map<String, Object> getDetailById(Integer id);

    List<MarketTopic> getRelatedById(Integer id);
}
