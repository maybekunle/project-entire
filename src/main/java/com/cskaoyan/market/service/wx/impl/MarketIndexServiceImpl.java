package com.cskaoyan.market.service.wx.impl;


import com.cskaoyan.market.db.domain.*;
import com.cskaoyan.market.db.mapper.*;
import com.cskaoyan.market.service.wx.MarketIndexService;
import com.cskaoyan.market.service.wx.MarketSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MarketIndexServiceImpl implements MarketIndexService {
    @Autowired
    private MarketAdMapper adMapper;
    @Autowired
    private MarketBrandMapper brandMapper;
    @Autowired
    private MarketCategoryMapper categoryMapper;
    @Autowired
    private MarketCouponMapper couponMapper;
    @Autowired
    private MarketGoodsMapper goodsMapper;
    @Autowired
    private MarketTopicMapper topicMapper;
    @Autowired
    private MarketSystemService systemService;


    @Override
    public Map<String, Object> getAllIndexInfo() {
        Map<String, String> systemInfo = systemService.getAllSystemInfo();
        String marketWxIndexBrand = systemInfo.get("market_wx_index_brand");
        String marketWxCatlogGoods = systemInfo.get("market_wx_catlog_goods");
        String marketWxCatlogList = systemInfo.get("market_wx_catlog_list");
        String marketWxIndexHot = systemInfo.get("market_wx_index_hot");
        String marketWxIndexNew = systemInfo.get("market_wx_index_new");
        String marketWxIndexTopic = systemInfo.get("market_wx_index_topic");



        Map<String, Object> indexMap = new HashMap<>();
        // banner
        MarketAdExample adExample = new MarketAdExample();
        adExample.createCriteria().andDeletedEqualTo(false);
        List<MarketAd> ads = adMapper.selectByExample(adExample);
        indexMap.put("banner", ads);

        // brandList
        MarketBrandExample brandExample = new MarketBrandExample();
        brandExample.createCriteria().andDeletedEqualTo(false);
        List<MarketBrand> marketBrands = brandMapper.selectByExampleSelective(brandExample, MarketBrand.Column.desc,
                MarketBrand.Column.floorPrice, MarketBrand.Column.id, MarketBrand.Column.name, MarketBrand.Column.picUrl);
        List<MarketBrand> marketBrandList = marketBrands.stream().
                limit(Integer.parseInt(marketWxIndexBrand)).collect(Collectors.toList());
        indexMap.put("brandList", marketBrandList);

        // channel
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        marketCategoryExample.createCriteria().andDeletedEqualTo(false).andLevelEqualTo("L1");
        List<MarketCategory> marketCategories = categoryMapper.selectByExampleSelective(marketCategoryExample, MarketCategory.Column.iconUrl,
                MarketCategory.Column.id, MarketCategory.Column.name);
        indexMap.put("channel", marketCategories);

        // couponList
        MarketCouponExample couponExample = new MarketCouponExample();
        couponExample.createCriteria().andDeletedEqualTo(false);
        List<MarketCoupon> marketCoupons = couponMapper.selectByExampleSelective(couponExample, MarketCoupon.Column.days, MarketCoupon.Column.desc,
                MarketCoupon.Column.discount, MarketCoupon.Column.id, MarketCoupon.Column.min, MarketCoupon.Column.name,
                MarketCoupon.Column.tag);
        List<MarketCoupon> couponList = marketCoupons.subList(0, 3);
        indexMap.put("couponList", couponList);

        // floorGoodsList
        List<Map<String, Object>> floorGoodsList = new ArrayList<>();
        for (int i = 0; i < Integer.parseInt(marketWxCatlogList); i++) {
            Map<String, Object> floorGoodsMap = new HashMap<>();
            MarketCategory marketCategory = marketCategories.get(i);
            floorGoodsMap.put("id", marketCategory.getId());
            floorGoodsMap.put("name", marketCategory.getName());
            MarketCategoryExample categoryExample = new MarketCategoryExample();
            categoryExample.or().andDeletedEqualTo(false).andPidEqualTo(marketCategory.getId());
            List<MarketCategory> L2Categories = categoryMapper.selectByExample(categoryExample);

            List<MarketGoods> goodsList = new ArrayList<>();
            for (MarketCategory l2Category : L2Categories) {
                MarketGoodsExample goodsExample = new MarketGoodsExample();
                goodsExample.createCriteria().andDeletedEqualTo(false).andCategoryIdEqualTo(l2Category.getId());

                goodsList.addAll(goodsMapper.selectByExample(goodsExample));

            }
            floorGoodsMap.put("goodsList", goodsList.subList(0, Integer.parseInt(marketWxCatlogGoods)));
            floorGoodsList.add(floorGoodsMap);

        }
        indexMap.put("floorGoodsList", floorGoodsList);

        // hotGoodsList
        MarketGoodsExample hotGoodsExample = new MarketGoodsExample();
        hotGoodsExample.createCriteria().andDeletedEqualTo(false).andIsHotEqualTo(true);
        List<MarketGoods> hotGoodsList = goodsMapper.selectByExample(hotGoodsExample);
        indexMap.put("hotGoodsList", hotGoodsList.subList(0, Integer.parseInt(marketWxIndexHot)));

        // newGoodsList
        MarketGoodsExample newGoodsExample = new MarketGoodsExample();
        newGoodsExample.createCriteria().andDeletedEqualTo(false).andIsNewEqualTo(true);
        List<MarketGoods> newGoodsList = goodsMapper.selectByExample(newGoodsExample);
        newGoodsList.subList(0, Integer.parseInt(marketWxIndexNew));
        indexMap.put("newGoodsList", newGoodsList.subList(0, Integer.parseInt(marketWxIndexNew)));

        // topicList
        MarketTopicExample topicExample = new MarketTopicExample();
        topicExample.or().andDeletedEqualTo(false);
        List<MarketTopic> topics = topicMapper.selectByExample(topicExample);
        List<MarketTopic> topicList = topics.stream().
                limit(Integer.parseInt(marketWxIndexTopic)).collect(Collectors.toList());
        indexMap.put("topicList", topicList);

        return indexMap;
    }
}
