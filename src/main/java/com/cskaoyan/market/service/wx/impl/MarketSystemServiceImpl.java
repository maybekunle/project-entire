package com.cskaoyan.market.service.wx.impl;


import com.cskaoyan.market.db.domain.MarketSystem;
import com.cskaoyan.market.db.domain.MarketSystemExample;
import com.cskaoyan.market.db.mapper.MarketSystemMapper;
import com.cskaoyan.market.service.wx.MarketSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MarketSystemServiceImpl implements MarketSystemService {
    @Autowired
    private MarketSystemMapper systemMapper;

    @Override
    public Map<String, String> getAllSystemInfo() {
        MarketSystemExample systemExample = new MarketSystemExample();
        systemExample.createCriteria().andDeletedEqualTo(false);
        List<MarketSystem> systems = systemMapper.selectByExample(systemExample);

        Map<String, String> systemMap = new HashMap<>();
        for (MarketSystem system : systems) {
            String keyName = system.getKeyName();
            String keyValue = system.getKeyValue();
            systemMap.put(keyName, keyValue);
        }

        return systemMap;
    }
}
