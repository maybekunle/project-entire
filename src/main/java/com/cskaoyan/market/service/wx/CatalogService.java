package com.cskaoyan.market.service.wx;

import com.cskaoyan.market.db.domain.MarketBrand;
import com.cskaoyan.market.vo.wx.CatalogCurrentVo;
import com.cskaoyan.market.vo.wx.CatalogIndexVo;

import java.util.List;

public interface CatalogService {

    CatalogIndexVo index();

    CatalogCurrentVo current(Integer id);
}
