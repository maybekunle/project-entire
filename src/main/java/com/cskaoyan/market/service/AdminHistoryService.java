package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketSearchHistory;

import java.util.List;

public interface AdminHistoryService {
    List<MarketSearchHistory> getHistories(Integer page, Integer limit, String userId, String keyword, String sort, String order);
}
