package com.cskaoyan.market.service.impl;


import com.cskaoyan.market.db.domain.MarketStorage;
import com.cskaoyan.market.db.mapper.MarketStorageMapper;
import com.cskaoyan.market.service.StorageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StorageServiceImpl implements StorageService {

    @Resource
    MarketStorageMapper marketStorageMapper;

    @Override
    public MarketStorage addOne(MarketStorage storage) {
        marketStorageMapper.insert(storage);
        return storage;
    }
}
