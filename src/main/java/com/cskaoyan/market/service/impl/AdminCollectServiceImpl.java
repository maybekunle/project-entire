package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketCollect;
import com.cskaoyan.market.db.domain.MarketCollectExample;
import com.cskaoyan.market.db.mapper.MarketCollectMapper;
import com.cskaoyan.market.service.AdminCollectService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminCollectServiceImpl implements AdminCollectService {

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Override
    public List<MarketCollect> getCollects(Integer page, Integer limit, String userId, String valueId, String sort, String order) {
        MarketCollectExample example = new MarketCollectExample();
        MarketCollectExample.Criteria or = example.or();
        if (StringUtils.isNumeric(userId)) {
            or.andUserIdEqualTo(Integer.parseInt(userId));
        }
        if (StringUtils.isNumeric(valueId)) {
            or.andValueIdEqualTo(Integer.parseInt(valueId));
        }
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        List<MarketCollect> collects = marketCollectMapper.selectByExample(example);

        return collects;
    }
}
