package com.cskaoyan.market.service.impl;


import com.cskaoyan.market.db.domain.MarketFootprint;
import com.cskaoyan.market.db.domain.MarketFootprintExample;
import com.cskaoyan.market.db.mapper.MarketFootprintMapper;
import com.cskaoyan.market.service.FootprintService;
import com.cskaoyan.market.vo.wx.FootprintVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FootprintServiceImpl implements FootprintService {

    @Autowired
    MarketFootprintMapper marketFootprintMapper;


    @Override
    public List<FootprintVo> queryFootprints(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<FootprintVo> footprintVos = marketFootprintMapper.selectGoodsDetail();

        return footprintVos;
    }

    @Override
    public void deleteFootprint(Integer id) {

        marketFootprintMapper.deleteByPrimaryKey(id);

    }

    @Override
    public List<MarketFootprint> getFootprints(Integer page, Integer limit,
                                               String userId, String goodsId, String sort, String order) {
        MarketFootprintExample example = new MarketFootprintExample();
        MarketFootprintExample.Criteria or = example.or();
        if (StringUtils.isNumeric(userId)) {
            or.andUserIdEqualTo(Integer.parseInt(userId));
        }
        if (StringUtils.isNumeric(goodsId)) {
            or.andGoodsIdEqualTo(Integer.parseInt(goodsId));
        }
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        List<MarketFootprint> footprints = marketFootprintMapper.selectByExample(example);
        return footprints;
    }
}
