package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.*;
import com.cskaoyan.market.db.mapper.MarketCouponMapper;
import com.cskaoyan.market.db.mapper.MarketCouponUserMapper;
import com.cskaoyan.market.db.mapper.MarketUserMapper;
import com.cskaoyan.market.service.CouponService;
import com.cskaoyan.market.vo.wx.CouponListVo;
import com.cskaoyan.market.vo.wx.CouponMyListVo;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;


@Service
@SuppressWarnings("all")
public class CouponServiceImpl implements CouponService {

    @Resource
    MarketCouponUserMapper marketCouponUserMapper;

    @Resource
    MarketCouponMapper marketCouponMapper;

    @Resource
    SecurityManager securityManager;

    @Resource
    MarketUserMapper marketUserMapper;

    @Override
    public List<CouponMyListVo> getMyList(@Param("status") Integer status, Integer page, Integer limit) {
        SecurityUtils.setSecurityManager(securityManager);
        Session session = SecurityUtils.getSubject().getSession();
        MarketUser user = (MarketUser) session.getAttribute("user");
        Integer userId = user.getId();
        PageHelper.startPage(page, limit);
        List<CouponMyListVo> couponVos = marketCouponUserMapper.selectCouponVos(status, userId);
        return couponVos;
    }

    @Override
    public boolean exchangeCoupon(String code) {
        SecurityUtils.setSecurityManager(securityManager);
        Session session = SecurityUtils.getSubject().getSession();
        MarketUser user = (MarketUser) session.getAttribute("user");
        Integer userId = user.getId();
        MarketCouponExample marketCouponExample = new MarketCouponExample();
        marketCouponExample.or().andCodeEqualTo(code);
        List<MarketCoupon> marketCoupons = marketCouponMapper.selectByExample(marketCouponExample);
        if (marketCoupons == null || marketCoupons.size() == 0) {
            return false;
        }
        MarketCoupon marketCoupon = marketCoupons.get(0);
        LocalDateTime startTime = marketCoupon.getStartTime();
        LocalDateTime endTime = marketCoupon.getEndTime();
        Integer couponId = marketCoupon.getId();


        //创建个人优惠券
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setCouponId(couponId);
        marketCouponUser.setUserId(userId);
        marketCouponUser.setDeleted(false);
        marketCouponUser.setAddTime(LocalDateTime.now());
        marketCouponUser.setUpdateTime(LocalDateTime.now());
        marketCouponUser.setUsedTime(null);
        marketCouponUser.setOrderId(null);
        marketCouponUser.setStartTime(startTime);
        marketCouponUser.setEndTime(endTime);
        marketCouponUser.setStatus((short) 0);
        marketCouponUserMapper.insert(marketCouponUser);

        return true;

    }

    @Override
    public List<CouponListVo> queryList(Integer page, Integer limit) {
        MarketCouponExample example = new MarketCouponExample();
        List<MarketCoupon> marketCoupons = marketCouponMapper.selectByExampleSelective(example);
        Page<CouponListVo> coupons = new Page<>();
        for (MarketCoupon marketCoupon : marketCoupons) {
            CouponListVo couponListVo = new CouponListVo();
            couponListVo.setDays(marketCoupon.getDays());
            couponListVo.setDesc(marketCoupon.getDesc());
            couponListVo.setDiscount(marketCoupon.getDiscount().doubleValue());
            couponListVo.setId(marketCoupon.getId());
            couponListVo.setMin(marketCoupon.getMin().doubleValue());
            couponListVo.setName(marketCoupon.getName());
            couponListVo.setTag(marketCoupon.getTag());
            coupons.add(couponListVo);
        }
        int size = marketCoupons.size();
        int pages = -1;
        if (size % limit == 0) {
            pages = size / limit;
        } else {
            pages = size / limit + 1;
        }
        coupons.setPages(pages);
        coupons.setPageSize(limit);
        coupons.setPageNum(page);
        coupons.setTotal(size);
        return coupons;
    }

    @Override
    public Boolean receiveCoupon(Integer couponId) {

        //查询用户id
        SecurityUtils.setSecurityManager(securityManager);
        Session session = SecurityUtils.getSubject().getSession();
        MarketUser user = (MarketUser) session.getAttribute("user");
        Integer userId = user.getId();

        //查询优惠券信息
        MarketCouponExample marketCouponExample = new MarketCouponExample();
        marketCouponExample.or().andIdEqualTo(couponId);
        List<MarketCoupon> marketCoupons = marketCouponMapper.selectByExample(marketCouponExample);
        if (marketCoupons == null || marketCoupons.size() == 0) {
            return false;
        }
        LocalDateTime startTime = marketCoupons.get(0).getStartTime();
        LocalDateTime endTime = marketCoupons.get(0).getEndTime();

        //创建个人优惠券
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setCouponId(couponId);
        marketCouponUser.setUserId(userId);
        marketCouponUser.setDeleted(false);
        marketCouponUser.setAddTime(LocalDateTime.now());
        marketCouponUser.setUpdateTime(LocalDateTime.now());
        marketCouponUser.setUsedTime(null);
        marketCouponUser.setOrderId(null);
        marketCouponUser.setStartTime(startTime);
        marketCouponUser.setEndTime(endTime);
        marketCouponUser.setStatus((short) 0);
        marketCouponUserMapper.insert(marketCouponUser);
        //50975371-462a-4078-9c7f-73ec7139f3be
        //50975371-462a-4078-9c7f-73ec7139f3be

        return true;
    }

    @Override
    public List<CouponMyListVo> getSelectList() {
        SecurityUtils.setSecurityManager(securityManager);
        MarketUser user = (MarketUser) SecurityUtils.getSubject().getSession().getAttribute("user");
        Integer userId = user.getId();
        Integer status = 0;
        List<CouponMyListVo> couponVos = marketCouponUserMapper.selectCouponVos(status, userId);
        Page<CouponMyListVo> page = new Page<>();
        for (CouponMyListVo couponVo : couponVos) {
            page.add(couponVo);
        }

        page.setTotal(couponVos.size());
        page.setPageNum(1);
        page.setPageSize(couponVos.size());
        page.setPages(1);
        return page;
    }


}
