package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.db.domain.MarketUserExample;
import com.cskaoyan.market.db.mapper.MarketUserMapper;
import com.cskaoyan.market.service.AdminUserService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Resource
    MarketUserMapper marketUserMapper;


    @Override
    public List<MarketUser> getUsers(String username, String mobile, Integer page, Integer limit, String sort, String order) {
        MarketUserExample example = new MarketUserExample();
        MarketUserExample.Criteria or = example.or();

        if (StringUtils.isNumeric(mobile)) {
            or.andMobileEqualTo(mobile);
        }
        if (!StringUtils.isEmpty(username)) {
            or.andUsernameEqualTo(username);
        }
        example.setOrderByClause(sort + " " + order);

        PageHelper.startPage(page, limit);
        List<MarketUser> users = marketUserMapper.selectByExample(example);

        return users;
    }

    @Override
    public List<MarketUser> getUser(String id) {
        int idValue = Integer.parseInt(id);
        MarketUserExample example = new MarketUserExample();
        example.or().andIdEqualTo(idValue);
        List<MarketUser> users = marketUserMapper.selectByExample(example);
        return users;
    }
}
