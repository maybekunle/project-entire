package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketKeyword;
import com.cskaoyan.market.db.domain.MarketKeywordExample;
import com.cskaoyan.market.db.domain.MarketSearchHistory;
import com.cskaoyan.market.db.domain.MarketSearchHistoryExample;
import com.cskaoyan.market.db.mapper.MarketKeywordMapper;
import com.cskaoyan.market.db.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.market.service.SearchService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class SearchServiceImpl implements SearchService {

    @Resource
    MarketSearchHistoryMapper searchHistoryMapper;

    @Resource
    MarketKeywordMapper keywordMapper;


    @Override
    public Map<String, Object> index() {
        Map<String, Object> info = new HashMap<>();
        MarketSearchHistoryExample historyExample = new MarketSearchHistoryExample();
        historyExample.or().andLogicalDeleted(true);
        List<MarketSearchHistory> histories = searchHistoryMapper.selectByExample(historyExample);
        info.put("historyKeywordList", histories);

        MarketKeyword keyword = keywordMapper.selectByPrimaryKey(4);
        info.put("defaultKeyword", keyword);

        MarketKeywordExample keywordExample = new MarketKeywordExample();
        List<MarketKeyword> keywords = keywordMapper.selectByExample(keywordExample);
        info.put("hotKeywordList", keywords);

        return info;


    }

    @Override
    public List<MarketSearchHistory> helper(String keyword) {
        MarketSearchHistoryExample historyExample = new MarketSearchHistoryExample();
        historyExample.or().andKeywordLike("%" + keyword + "%").andLogicalDeleted(true);

        List<MarketSearchHistory> histories = searchHistoryMapper.selectByExample(historyExample);

        return histories;
    }

    @Override
    public void clearHistory() {
        MarketSearchHistoryExample example = new MarketSearchHistoryExample();
        MarketSearchHistory history = new MarketSearchHistory();
        history.setDeleted(true);
        searchHistoryMapper.updateByExampleSelective(history, example);
    }
}
