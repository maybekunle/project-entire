package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketFeedback;
import com.cskaoyan.market.db.domain.MarketFeedbackExample;
import com.cskaoyan.market.db.mapper.MarketFeedbackMapper;
import com.cskaoyan.market.service.AdminFeedBackService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminFeedBackServiceImpl implements AdminFeedBackService {

    @Resource
    MarketFeedbackMapper marketFeedbackMapper;

    @Override
    public List<MarketFeedback> getFeedbacks(Integer page, Integer limit, String username, String sort, String order, String id) {
        MarketFeedbackExample example = new MarketFeedbackExample();
        MarketFeedbackExample.Criteria or = example.or();
        if (!StringUtils.isEmpty(username)) {
            or.andUsernameEqualTo(username);
        }
        if (StringUtils.isNumeric(id)) {
            or.andIdEqualTo(Integer.parseInt(id));
        }
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        List<MarketFeedback> feedbacks = marketFeedbackMapper.selectByExample(example);
        return feedbacks;
    }
}
