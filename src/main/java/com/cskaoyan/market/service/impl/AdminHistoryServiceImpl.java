package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketSearchHistory;
import com.cskaoyan.market.db.domain.MarketSearchHistoryExample;
import com.cskaoyan.market.db.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.market.service.AdminHistoryService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@SuppressWarnings("all")
public class AdminHistoryServiceImpl implements AdminHistoryService {

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Override
    public List<MarketSearchHistory> getHistories(Integer page, Integer limit, String userId, String keyword, String sort, String order) {
        MarketSearchHistoryExample example = new MarketSearchHistoryExample();
        MarketSearchHistoryExample.Criteria or = example.or();
        if (StringUtils.isNumeric(userId)) {
            or.andUserIdEqualTo(Integer.parseInt(userId));
        }
        if (!StringUtils.isEmpty(keyword)) {
            or.andKeywordLike("%" + keyword + "%");
        }

        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        List<MarketSearchHistory> histories = marketSearchHistoryMapper.selectByExample(example);

        return histories;
    }
}
