package com.cskaoyan.market.service.impl;

import com.cskaoyan.market.db.domain.MarketAddress;
import com.cskaoyan.market.db.domain.MarketAddressExample;
import com.cskaoyan.market.db.mapper.MarketAddressMapper;
import com.cskaoyan.market.service.AdminAddressService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminAddressServiceImpl implements AdminAddressService {

    @Resource
    MarketAddressMapper marketAddressMapper;

    @Override
    public List<MarketAddress> getAddresses(Integer page, Integer limit, String name, String userId, String sort, String order) {
        MarketAddressExample example = new MarketAddressExample();
        MarketAddressExample.Criteria or = example.or();
        if (!StringUtils.isEmpty(name)) {
            or.andNameEqualTo(name);
        }
        if (StringUtils.isNumeric(userId)) {
            or.andUserIdEqualTo(Integer.parseInt(userId));
        }
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        List<MarketAddress> addresses = marketAddressMapper.selectByExample(example);
        return addresses;
    }
}
