package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketSearchHistory;

import java.util.List;
import java.util.Map;

public interface SearchService {
    Map<String, Object> index();

    List<MarketSearchHistory> helper(String keyword);

    void clearHistory();

}
