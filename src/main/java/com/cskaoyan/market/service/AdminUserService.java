package com.cskaoyan.market.service;

import com.cskaoyan.market.db.domain.MarketUser;

import java.util.List;

public interface AdminUserService {
    List<MarketUser> getUsers(String username, String mobile, Integer page, Integer limit, String sort, String order);

    List<MarketUser> getUser(String id);
}
