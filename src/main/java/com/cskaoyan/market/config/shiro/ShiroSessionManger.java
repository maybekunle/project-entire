package com.cskaoyan.market.config.shiro;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;


@Component
public class ShiroSessionManger extends DefaultWebSessionManager {

    private static final String WX_MARKET_TOKEN = "X-CskaoyanMarket-Token";

    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        String token = WebUtils.toHttp(request).getHeader(WX_MARKET_TOKEN);
        if (!StringUtils.isEmpty(token)) {
            return token;
        }
        return super.getSessionId(request, response);
    }
}
