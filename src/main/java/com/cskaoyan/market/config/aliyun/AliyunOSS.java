package com.cskaoyan.market.config.aliyun;

import lombok.Data;

@Data
public class AliyunOSS {
    private String bucketName;

    private String endPoint;

}
