package com.cskaoyan.market.config.aliyun;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aliyun")
@Data
public class AliyunConfig {

    private String accessKeyId;

    private String accessKeySecret;

    private AliyunOSS oss;


}
