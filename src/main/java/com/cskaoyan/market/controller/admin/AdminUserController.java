package com.cskaoyan.market.controller.admin;


import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.service.AdminUserService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * /admin/user/list?page=1&limit=20&username=1&mobile=1&userId=&sort=add_time&order=desc
 * GET
 */
@RestController
@RequestMapping("/admin/user")
public class AdminUserController {

    @Resource
    AdminUserService adminUserService;

    @GetMapping("/list")
    public Object getUsers(String username, String mobile, Integer page, Integer limit, String sort, String order) {

        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }
        List<MarketUser> users = adminUserService.getUsers(username, mobile, page, limit, sort, order);
        Object okList = ResponseUtil.okList(users);
        return okList;
    }

    @GetMapping("detail")
    public Object getUser(String id) {
        if (!StringUtils.isNumeric(id)) {
            return ResponseUtil.fail(400, "参数不合法");
        }
        List<MarketUser> users = adminUserService.getUser(id);
        if (users == null || users.size() == 0) {
            return ResponseUtil.ok();
        }
        MarketUser user = users.get(0);
        Object ok = ResponseUtil.ok(user);
        return ok;
    }
}
