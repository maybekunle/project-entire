package com.cskaoyan.market.controller.admin;


import com.cskaoyan.market.db.domain.MarketCollect;
import com.cskaoyan.market.service.AdminCollectService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * /admin/collect/list?page=1&limit=20&userId=1&valueId=1&sort=add_time&order=desc
 */
@RestController
public class AdminCollectController {

    @Autowired
    AdminCollectService adminCollectService;

    @GetMapping("admin/collect/list")
    public Object getCollects(Integer page, Integer limit, String userId, String valueId,
                              String sort, String order) {
        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }
        List<MarketCollect> collects = adminCollectService.getCollects(page, limit, userId, valueId
                , sort, order);
        Object okList = ResponseUtil.okList(collects);

        return okList;
    }


}
