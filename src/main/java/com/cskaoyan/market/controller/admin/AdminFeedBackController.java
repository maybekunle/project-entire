package com.cskaoyan.market.controller.admin;

import com.cskaoyan.market.db.domain.MarketFeedback;
import com.cskaoyan.market.service.AdminFeedBackService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * admin/feedback/list?page=1&limit=20&username=1&sort=add_time&order=desc&id=1
 */
@RestController
public class AdminFeedBackController {

    @Autowired
    AdminFeedBackService adminFeedBackService;

    @GetMapping("/admin/feedback/list")
    public Object getFeedBacks(Integer page, Integer limit, String username,
                               String sort, String order, String id) {
        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }
        List<MarketFeedback> feedbacks = adminFeedBackService.getFeedbacks(page,
                limit, username, sort, order, id);
        Object okList = ResponseUtil.okList(feedbacks);
        return okList;
    }

}
