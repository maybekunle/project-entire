package com.cskaoyan.market.controller.admin;


import com.cskaoyan.market.db.domain.MarketSearchHistory;
import com.cskaoyan.market.service.AdminHistoryService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * admin/history/list?page=1&limit=20&userId=1&keyword=1&sort=add_time&order=desc
 */
@RestController
public class AdminHistoryController {

    @Autowired
    AdminHistoryService adminHistoryService;


    @GetMapping("admin/history/list")
    public Object getHistories(Integer page, Integer limit, String userId,
                               String keyword, String sort, String order) {

        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }

        List<MarketSearchHistory> histories = adminHistoryService.getHistories(
                page, limit, userId, keyword, sort, order);
        Object okList = ResponseUtil.okList(histories);

        return okList;

    }
}
