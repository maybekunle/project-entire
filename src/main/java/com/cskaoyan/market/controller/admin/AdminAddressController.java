package com.cskaoyan.market.controller.admin;


import com.cskaoyan.market.db.domain.MarketAddress;
import com.cskaoyan.market.service.AdminAddressService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class AdminAddressController {

    @Resource
    AdminAddressService adminAddressService;

    /**
     * admin/address/list?page=1&limit=20&name=1&userId=1&sort=add_time&order=desc
     *
     * @return
     */
    @GetMapping("/admin/address/list")
    public Object getAddresses(Integer page, Integer limit, String name, String userId, String sort, String order) {

        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }
        List<MarketAddress> addresses = adminAddressService.getAddresses(page, limit, name, userId, sort, order);
        Object okList = ResponseUtil.okList(addresses);
        return okList;
    }

}
