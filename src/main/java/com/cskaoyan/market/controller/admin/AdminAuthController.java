package com.cskaoyan.market.controller.admin;


import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.service.wx.AuthService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.UserInfoVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/admin/auth")
public class AdminAuthController {

    @Autowired
    AuthService authService;

    @Autowired
    SecurityManager securityManager;


    @PostMapping("login")
    public Object login(@RequestBody MarketUser loginUser) {
        String username = loginUser.getUsername();
        String password = loginUser.getPassword();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();

        UserInfoVo adminInfoVo = null;
        try {
            subject.login(token);
            adminInfoVo = authService.selectAdminInfoByUsername(username);

        } catch (Exception e) {
            return ResponseUtil.failedLogin();
        }
        Map<String, Object> result = new HashMap<>();
        result.put("userInfo", adminInfoVo);
        result.put("token", session.getId());
        return ResponseUtil.ok(result);
    }
}
