package com.cskaoyan.market.controller.admin;

import com.cskaoyan.market.db.domain.MarketFootprint;
import com.cskaoyan.market.service.FootprintService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * /admin/footprint/list?page=1&limit=20&userId=1&goodsId=1&sort=add_time&order=desc
 */
@RestController
public class AdminFootprintController {

    @Autowired
    FootprintService footprintService;


    @GetMapping("/admin/footprint/list")
    public Object getFootprints(Integer page, Integer limit, String userId,
                                String goodsId, String sort, String order) {
        if (StringUtils.isEmpty(sort)) {
            sort = "add_time";
        }
        if (StringUtils.isEmpty(order)) {
            order = "desc";
        }

        List<MarketFootprint> footprints = footprintService.getFootprints(page, limit, userId,
                goodsId, sort, order);
        Object okList = ResponseUtil.okList(footprints);
        return okList;
    }

}
