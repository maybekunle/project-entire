package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.db.domain.MarketSearchHistory;
import com.cskaoyan.market.service.SearchService;
import com.cskaoyan.market.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/search")
public class SearchController {

    @Autowired
    SearchService searchService;

    @GetMapping("/index")
    public Object index() {
        Map<String, Object> info = searchService.index();
        Object ok = ResponseUtil.ok(info);
        return ok;
    }

    /**
     * /wx/search/helper?keyword=11
     */
    @GetMapping("/helper")
    public Object helper(String keyword) {
        List<MarketSearchHistory> histories = searchService.helper(keyword);
        List<String> keywords = new ArrayList<>();
        if (histories.size() == 0) {
            return ResponseUtil.ok(keywords);
        }
        for (MarketSearchHistory history : histories) {
            keywords.add(history.getKeyword());
        }
        Object ok = ResponseUtil.ok(keywords);
        return ok;

    }

    @PostMapping("/clearhistory")
    public Object clearHistory() {
        searchService.clearHistory();

        return ResponseUtil.ok();
    }

}
