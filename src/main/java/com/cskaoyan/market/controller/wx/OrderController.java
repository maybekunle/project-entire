package com.cskaoyan.market.controller.wx;

import com.cskaoyan.market.db.domain.MarketOrderGoods;
import com.cskaoyan.market.service.wx.OrderService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.MarketOrderVo;
import com.cskaoyan.market.vo.wx.OrderDetailVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/order")
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    SecurityManager securityManager;

    @GetMapping("list")
    public Object list(Integer showType, Integer page, Integer limit) {
        List<MarketOrderVo> orderList = orderService.list(showType, page, limit);
        return ResponseUtil.okList(orderList);

    }


    @PostMapping("comment")
    public Object comment(@RequestBody Map<String, Object> query) {
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        String username = (String) session.getAttribute("username");

        String content = (String) query.get("content");
        Boolean hasPicture = (Boolean) query.get("hasPicture");
        Integer orderGoodsId = (Integer) query.get("orderGoodsId");
        String[] picUrls;
        if (hasPicture) {
            picUrls = (String[]) query.get("picUrls");
        } else {
            picUrls = new String[]{};
        }
        Integer star = (Integer) query.get("star");
        int code = orderService.comment(username, content, hasPicture, orderGoodsId, picUrls, star);
        if (code == 200) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.failedComment();


    }

    @GetMapping("detail")
    public Object detail(Integer orderId) {
        OrderDetailVo detailVo = orderService.detail(orderId);
        return ResponseUtil.ok(detailVo);
    }

    @GetMapping("goods")
    public Object goods(Integer orderId) {
        MarketOrderGoods orderGoods = orderService.goods(orderId);
        return ResponseUtil.ok(orderGoods);
    }

    @PostMapping("refund")
    public Object refund(@RequestBody Map<String, Object> query) {
        Object orderId = query.get("orderId");
        int code = orderService.refund((Integer) orderId);
        return code == 200 ? ResponseUtil.ok() : ResponseUtil.failedComment();
    }

    @PostMapping("cancel")
    public Object cancel(@RequestBody Map<String, Object> query) {
        Object orderId = query.get("orderId");
        int code = orderService.cancel((Integer) orderId);
        return ResponseUtil.ok();
    }

    @PostMapping("delete")
    public Object delete(@RequestBody Map<String, Object> query) {
        Object orderId = query.get("orderId");
        int code = orderService.delete((Integer) orderId);
        return ResponseUtil.ok();
    }

    @PostMapping("confirm")
    public Object confirm(@RequestBody Map<String, Object> query) {
        Object orderId = query.get("orderId");
        int code = orderService.confirm((Integer) orderId);
        return ResponseUtil.ok();
    }
}
