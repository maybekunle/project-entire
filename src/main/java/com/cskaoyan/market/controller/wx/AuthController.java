package com.cskaoyan.market.controller.wx;

import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.service.wx.AuthService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.UserInfoVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/wx/auth")
public class AuthController {
    @Autowired
    AuthService authService;
    @Autowired
    SecurityManager securityManager;


    @PostMapping("login")
    public Object login(@RequestBody MarketUser loginUser) {
        UsernamePasswordToken token = new UsernamePasswordToken(loginUser.getUsername(), loginUser.getPassword());
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();

        UserInfoVo userInfoVo = null;
        try {
            subject.login(token);
            userInfoVo = authService.selectUserInfoByUsername(loginUser.getUsername());

            session.setAttribute("username", loginUser.getUsername());
            session.setAttribute("sessionId", session.getId());
        } catch (Exception e) {
            return ResponseUtil.failedLogin();
        }

        //会话存入用户信息 by Lee
        String username = loginUser.getUsername();
        MarketUser user = authService.selectUser(username);
        session.setAttribute("user", user);


        Map<String, Object> result = new HashMap<>();
        result.put("userInfo", userInfoVo);
        result.put("token", session.getId());
        return ResponseUtil.ok(result);
    }

    @PostMapping("logout")
    public Object logout() {
        SecurityUtils.getSubject().logout();
        return ResponseUtil.ok();

    }
}
