package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.db.domain.MarketTopic;
import com.cskaoyan.market.service.wx.MarketTopicService;
import com.cskaoyan.market.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/topic")
public class WxTopicController {
    @Autowired
    private MarketTopicService topicService;

    @GetMapping("/list")
    public Object list(Integer page, Integer limit){
        List<MarketTopic> topicList = topicService.list(page, limit);
        return ResponseUtil.okList(topicList);
    }

    @GetMapping("/detail")
    public Object detail(Integer id){
        Map<String, Object> detailMap = topicService.getDetailById(id);
        return ResponseUtil.ok(detailMap);
    }

    @GetMapping("/related")
    public Object related(Integer id){
        List<MarketTopic> topicList = topicService.getRelatedById(id);
        return ResponseUtil.okList(topicList);
    }
}
