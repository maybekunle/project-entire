package com.cskaoyan.market.controller.wx;



import com.cskaoyan.market.service.FootprintService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.FootprintVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/*
 * /wx/footprint/list?page=1&limit=10
 */
@RestController
@RequestMapping("/wx/footprint")
public class FootprintController {

    @Autowired
    FootprintService footprintService;

    @GetMapping("list")
    public Object queryFootprint(Integer page, Integer limit) {
        List<FootprintVo> footprints = footprintService.queryFootprints(page, limit);
        Object okList = ResponseUtil.okList(footprints);
        return okList;
    }

    @PostMapping("delete")
    public Object deleteFootprint(@RequestBody Map<String, Integer> params) {
        Integer id = params.get("id");
        footprintService.deleteFootprint(id);
        return ResponseUtil.ok();
    }
}
