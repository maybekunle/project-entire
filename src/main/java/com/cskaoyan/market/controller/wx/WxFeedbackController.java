package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.db.domain.MarketFeedback;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.service.wx.MarketFeedbackService;
import com.cskaoyan.market.util.ResponseUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/wx/feedback")
public class WxFeedbackController {
    @Autowired
    private MarketFeedbackService feedbackService;


    @RequestMapping("/submit")
    @RequiresAuthentication
    public Object submit(@RequestBody MarketFeedback feedback, HttpSession session){
        MarketUser user = (MarketUser) session.getAttribute("user");

        boolean rs = feedbackService.submit(feedback, user);
        if(rs){
            return ResponseUtil.ok();
        }else {
            return ResponseUtil.fail();
        }
    }
}
