package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.service.CouponService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.CouponListVo;
import com.cskaoyan.market.vo.wx.CouponMyListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/coupon")
public class CouponController {

    @Autowired
    CouponService couponService;

    @GetMapping("/mylist")
    public Object getMyList(Integer status, Integer page, Integer limit) {
        List<CouponMyListVo> coupons = couponService.getMyList(status, page, limit);
        Object okList = ResponseUtil.okList(coupons);
        return okList;
    }

    @PostMapping("/exchange")
    public Object exchangeCoupon(@RequestBody Map<String,String> map) {
        String code = map.get("code");
        boolean b = couponService.exchangeCoupon(code);
        if (b) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail(500,"兑换失败");
    }


    /**
     * /wx/coupon/list?page=1&limit=10
     *
     * @return
     */
    @GetMapping("/list")
    public Object list(Integer page, Integer limit) {
        List<CouponListVo> coupons = couponService.queryList(page, limit);
        Object okList = ResponseUtil.okList(coupons);
        return okList;
    }

    @PostMapping("/receive")
    public Object receive(@RequestBody Map<String, Integer> map) {
        Integer couponId = map.get("couponId");
        Boolean result = couponService.receiveCoupon(couponId);
        if (result) {
            return ResponseUtil.ok();
        }
        return ResponseUtil.fail();
    }

    @GetMapping("/selectlist")
    public Object selectList() {
        List<CouponMyListVo> list = couponService.getSelectList();
        Object okList = ResponseUtil.okList(list);
        return okList;
    }

}
