package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.db.domain.MarketComment;
import com.cskaoyan.market.db.domain.MarketUser;
import com.cskaoyan.market.service.wx.MarketCommentService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.MarketCommentVo;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/wx/comment")
public class WxCommentController {
    @Autowired
    private MarketCommentService commentService;

    // valueId=294&type=1&showType=0&page=1&limit=5
    @GetMapping("/list")
    public Object list(Integer valueId, Integer type, Integer showType, String page, String limit){
        Integer currentPage;
        try {
            currentPage = Integer.parseInt(page);
        } catch (NumberFormatException e) {
            currentPage = 1;
        }
        Integer limitSize;
        try {
            limitSize = Integer.parseInt(limit);
        } catch (NumberFormatException e) {
            limitSize = 5;
        }

        List<MarketCommentVo> commentVos = commentService.list(valueId, type, showType, currentPage, limitSize);
        List<MarketComment> listForPage = commentService.listForPage(valueId, type, showType, currentPage, limitSize);
        return ResponseUtil.okList(commentVos, listForPage);
    }

    @GetMapping("/count")
    public Object count(Integer valueId, Integer type){
        Map<String, Integer> count = commentService.count(valueId, type);

        return ResponseUtil.ok(count);

    }


    @PostMapping("/post")
    @RequiresAuthentication
    public Object post(@RequestBody MarketComment comment, HttpSession session){
        MarketUser user = (MarketUser) session.getAttribute("user");
        MarketComment commentReturn = commentService.post(comment, user);
        return ResponseUtil.ok(commentReturn);
    }
}
