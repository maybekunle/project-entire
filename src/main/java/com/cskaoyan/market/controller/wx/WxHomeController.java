package com.cskaoyan.market.controller.wx;


import com.cskaoyan.market.service.wx.MarketIndexService;
import com.cskaoyan.market.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/wx/home")
public class WxHomeController {
    @Autowired
    private MarketIndexService indexService;

    @GetMapping("/index")

    public Object index(){
        Map<String, Object> indexInfo = indexService.getAllIndexInfo();
        return ResponseUtil.ok(indexInfo);
    }
}
