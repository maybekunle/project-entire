package com.cskaoyan.market.controller.wx;

import com.cskaoyan.market.db.domain.MarketBrand;

import com.cskaoyan.market.service.wx.BrandService;
import com.cskaoyan.market.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wx/brand")
public class BrandController {
    @Autowired
    BrandService brandService;
    @GetMapping("list")
    public Object index(Integer page, Integer limit) {
        List<MarketBrand> result = brandService.list(page, limit);
        return ResponseUtil.okList(result);
    }
    @GetMapping("detail")
    public Object detail(Integer id){
        MarketBrand brand = brandService.detail(id);
        return ResponseUtil.ok(brand);
    }


}
