package com.cskaoyan.market.controller.wx;

import com.cskaoyan.market.service.wx.CatalogService;
import com.cskaoyan.market.util.ResponseUtil;
import com.cskaoyan.market.vo.wx.CatalogCurrentVo;
import com.cskaoyan.market.vo.wx.CatalogIndexVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wx/catalog")
public class CatalogController {
    @Autowired
    CatalogService catalogService;
    @GetMapping("index")
    public Object index(){
       CatalogIndexVo catalogIndexVo = catalogService.index();
       return ResponseUtil.ok(catalogIndexVo);
    }
    @GetMapping("current")
    public Object current(Integer id){
        CatalogCurrentVo catalogCurrentVo = catalogService.current(id);
        return ResponseUtil.ok(catalogCurrentVo);
    }

}
