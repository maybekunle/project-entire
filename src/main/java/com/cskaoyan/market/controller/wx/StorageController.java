package com.cskaoyan.market.controller.wx;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.cskaoyan.market.config.aliyun.AliyunConfig;
import com.cskaoyan.market.db.domain.MarketStorage;
import com.cskaoyan.market.service.StorageService;
import com.cskaoyan.market.util.ResponseUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
public class StorageController {

    @Resource
    StorageService storageService;

    @Resource
    AliyunConfig aliyunConfig;


    @PostMapping("/wx/storage/upload")
    public Object upload(MultipartFile file) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpg");
        OSS ossClient = new OSSClientBuilder().build(aliyunConfig.getOss().getEndPoint(),
                aliyunConfig.getAccessKeyId(), aliyunConfig.getAccessKeySecret());

        //获取图片参数
        String name = file.getOriginalFilename();
        int size = (int) file.getSize();
        String type = file.getContentType();
        int index = name.indexOf(".");
        String suffix = name.substring(index);
        String key = UUID.randomUUID().toString() + suffix;

        //存入阿里云
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        objectMetadata.setContentDisposition("inline;filename=" + key);
        ossClient.putObject(aliyunConfig.getOss().getBucketName(),
                key, inputStream, objectMetadata);
        ossClient.shutdown();
        String bucketName = aliyunConfig.getOss().getBucketName();
        String endPoint = aliyunConfig.getOss().getEndPoint();
        String url = "https://" + bucketName + "." + endPoint + "/" + key;


        //创建对象写入数据库
        MarketStorage storage = new MarketStorage();
        storage.setKey(key);
        storage.setDeleted(false);
        storage.setUpdateTime(LocalDateTime.now());
        storage.setAddTime(LocalDateTime.now());
        storage.setName(name);
        storage.setType(type);
        storage.setSize(size);
        storage.setUrl(url);

        //返回图片信息
        MarketStorage marketStorage = storageService.addOne(storage);
        Object ok = ResponseUtil.ok(marketStorage);
        return ok;
    }

}
