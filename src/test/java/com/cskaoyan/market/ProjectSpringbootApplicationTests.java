package com.cskaoyan.market;

import com.cskaoyan.market.db.domain.MarketTopic;
import com.cskaoyan.market.db.mapper.MarketTopicMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProjectSpringbootApplicationTests {

    @Autowired
    private MarketTopicMapper mapper;
    @Test
    void contextLoads() {
        List<MarketTopic> topics = mapper.selectByExample(null);
        System.out.println(topics);
    }

}
